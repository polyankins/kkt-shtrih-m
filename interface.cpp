/***************************************************************************
                          interface.cpp  -  description
                             -------------------
    begin                : Tue Jan 15 2002
    copyright            : (C) 2002 by Igor V. Youdytsky
    email                : Pitcher@gw.tander.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "barcodes/barcode.h"
//#include "images/bmp.cpp"
#include "images/bitmap/bitmap_image.hpp"
//const int width = 255, height = 255;

#include <iostream>
#include <bitset>
#include <algorithm>
#include <vector>
#include <sstream>

#include <malloc.h>


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>


#include "errdefs.h"
#include "conn.h"
#include "drvfr.h"
#include "options.h"
#include "interface.h"

/*
#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

using namespace log4cxx;
using namespace log4cxx::helpers;

extern LoggerPtr __glb_logger;
extern LoggerPtr __glb_logger_result;
*/
using namespace std;

/**********************************************************
 * Local functions & procedures                           *
 **********************************************************/

int errhand(int , void *);
int evalint(unsigned char*, int);
int64_t evalint64(unsigned char*, int);
void evaldate(unsigned char*, struct tm*);
void DefineECRModeDescription(void);

/**********************************************************
 * Local static variables                                 *
 **********************************************************/
static int initialized;
//static int __glb_connected;
static fr_prop fr;
static fr_func fn = {
    Connect,
    DisConnect,
    Beep,
    Buy,
    CancelCheck,
    CashIncome,
    CashOutcome,
    Charge,
    CheckSubTotal,
    OpenCheck,
    CloseCheck,
    ConfirmDate,
    ContinuePrinting,
    Correction,
    CutCheck,
    DampRequest,
    Discount,
    DozeOilCheck,
    Draw,
    DrawExt,
    EjectSlipDocument,
    EKLZDepartmentReportInDatesRange,
    EKLZDepartmentReportInSessionsRange,
    EKLZJournalOnSessionNumber,
    EKLZSessionReportInDatesRange,
    EKLZSessionReportInSessionRange,
    FeedDocument,
    Fiscalization,
    FiscalReportForDatesRange,
    FiscalReportForSessionRange,
    GetData,
    GetDeviceMetrics,
    GetExchangeParam,
    GetFieldStruct,
    GetFiscalizationParameters,
    GetFMRecordsSum,
    GetShortStatus,
    GetECRStatus,
    GetFNStatus,
    GetLastFMRecordDate,
    GetLiterSumCounter,
    GetCashReg,
    GetOperationReg,
    GetRangeDatesAndSessions,
    GetRKStatus,
    GetTableStruct,
    InitFM,
    InitTable,
    InterruptDataStream,
    InterruptFullReport,
    InterruptTest,
    LaunchRK,
    LoadLineData,
    LoadLineDataExt,
    OilSale,
    OpenDrawer,
    PrintBarCode,
    PrintBmpImage,
    PrintLine,
    PrintBarCodeMultidimensional,
    PrintBarCode128ByLine,
    PrintQRCode,
    PrintDocumentTitle,
    PrintOperationReg,
    PrintReportWithCleaning,
    PrintReportWithoutCleaning,
    PrintReportWithCleaningToBuffer,
    PrintString,
    PrintWideString,
    ReadEKLZDocumentOnKPK,
    ReadEKLZSessionTotal,
    ReadLicense,
    ReadTable,
    ResetAllTRK,
    ResetRK,
    ResetSettings,
    ResetSummary,
    ReturnBuy,
    ReturnSale,
    Sale,
    SetDate,
    SetDozeInMilliliters,
    SetDozeInMoney,
    SetExchangeParam,
    SetPointPosition,
    SetRKParameters,
    SetSerialNumber,
    SetTime,
    StopEKLZDocumentPrinting,
    StopRK,
    Storno,
    StornoCharge,
    StornoDiscount,
    SummOilCheck,
    Test,
    WriteLicense,
    WriteTable,
    OpenShift,
    CancelPrint,
    ResetFnState,
    GetInformExchangeStatus,
    DownloadData,
    SetPrintReceiptToOut,
    SetPrintReportToOut,
    GetPrintReceiptToOut,
    GetPrintReportToOut,
    GetBufferSize,
    ReadBlockFromBuffer,
    PrintFromBuffer,
    &fr
};
//-----------------------------------------------------------------------------

fr_func *drvfrInitialize(void) 
{
    std::stringstream log_ss;
    log_ss << "drvfrInitialize " << std::endl;
    if (!initialized) {
        log_ss << " not init " << std::endl;
        fr.SerialNumber = (char*) malloc(16);
        fr.License = (char*) malloc(13);
        fr.RNM = (char*) malloc(13);
        fr.INN = (char*) malloc(13);
        fr.ECRSoftVersion = (char*) malloc(4);
        fr.FMSoftVersion = (char*) malloc(4);
        fr.DataBlock = (unsigned char*) malloc(32);
        fr.ValueOfFieldString = (char*) malloc(41);
        fr.FieldName = (char*) malloc(41);
        fr.LineData = (unsigned char*) malloc(256); // 41 old
        fr.LineDataExt = (unsigned char*) malloc(256);
        fr.ResultCodeDescription = NULL;
        fr.DownloadData = (unsigned char*) malloc(256);
        fr.ECRModeDescription = NULL;
        fr.ECRSubmodeDescription = NULL;
        initialized = true;
        
        if( __glb_args.debug > 0 ){
            log_ss << "    prepared args " << std::endl
               << "    __glb_args.debug = " << __glb_args.debug << std::endl
               << "    __glb_args.debug_level = " << __glb_args.debug_level << std::endl
               << "    __glb_args.port = " << __glb_args.port << std::endl
               << "    __glb_args.baudrate = " << __glb_args.baudrate << std::endl
               << "    __glb_args.flow = " << __glb_args.flow << std::endl
               << "    __glb_args.password = " << __glb_args.password << std::endl
               << "    __glb_args.password_ti = " << __glb_args.password_ti << std::endl
               << "    __glb_args.password_sc = " << __glb_args.password_sc << std::endl
               << "    __glb_args.password_admin = " << __glb_args.password_admin << std::endl
               << "    __glb_args.use_auto_timeout = " << __glb_args.use_auto_timeout << std::endl
               << "    __glb_args.step_auto_timeout = " << __glb_args.step_auto_timeout << std::endl
               << "    __glb_args.max_auto_timeout = " << __glb_args.max_auto_timeout << std::endl
               << "    __glb_args.max_connect_tries = " << __glb_args.max_connect_tries << std::endl
               << "    __glb_args.max_execute_tries = " << __glb_args.max_execute_tries << std::endl
               << "    __glb_args.max_resend_tries = " << __glb_args.max_resend_tries << std::endl
               << "    __glb_args.max_tries = " << __glb_args.max_tries << std::endl
               << "    __glb_args.timeout = " << __glb_args.timeout << std::endl
               << "    __glb_args.autoz = " << __glb_args.autoz << std::endl
               << "    __glb_args.cut_off = " << __glb_args.cut_off << std::endl
               << "    __glb_args.cmd = " << __glb_args.cmd << std::endl;
        }
        fr.ComPortString = (char *)"/dev/ttyACM0";
        
        if( __glb_args.port != NULL && strlen(__glb_args.port) > 0 ){
            fr.ComPortString = (char *)__glb_args.port;
        }
        log_ss << " ComPortString " << fr.ComPortString << std::endl;
        
        std::stringstream ss;
        ss << __glb_args.password;
        ss >> fr.Password;
        ss.clear();
        
        fr.PasswordAdmin = 30;
        if( __glb_args.password_admin != NULL ){
            sscanf(__glb_args.password_admin,"%d",&fr.PasswordAdmin);
        }
        log_ss << " Password Admin " << fr.PasswordAdmin << std::endl;
        
        fr.Password = 30;
        if( __glb_args.password != NULL ){
            sscanf(__glb_args.password,"%d",&fr.Password);
        }
        log_ss << " Password Operator " << fr.Password << std::endl;
        
        fr.Timeout = __glb_args.timeout;
        log_ss << " Timeout " << fr.Timeout << std::endl;
        
        fr.BaudRate = get_num_baudrate(__glb_args.baudrate);
        log_ss << " BaudRate " << fr.BaudRate << std::endl;
    };
    log_ss << " initialized " << std::endl;
    
    //LOG4CXX_DEBUG( __glb_logger, log_ss.str() );
    log_ss.clear();
    
    
    return &fn;
}
//-----------------------------------------------------------------------------

int errhand(int comm, answer *a) 
{
    fr.ResultCode = a->buff[a->cmd_len];
    fr.ResultCodeDescription = strdup(errmsg[fr.ResultCode]);
    if (fr.ResultCode != 0) {
        //LOG4CXX_ERROR( __glb_logger, "!!! ERROR[" << std::dec << fr.ResultCode << "] - " << fr.ResultCodeDescription << " !!!");
    } else {
        //LOG4CXX_DEBUG( __glb_logger, "*** SUCCESS ***" );
    }
    return fr.ResultCode;
}
//-----------------------------------------------------------------------------

int evalint(unsigned char *str, int len) 
{
    int result = 0;
    while (len--) {
        result <<= 8;
        result += str[len];
    };
    return result;
}
//-----------------------------------------------------------------------------

int64_t evalint64(unsigned char *str, int len) 
{
    int64_t result = 0;
    while (len--) {
        result <<= 8;
        result += str[len];
    };
    return result;
}
//-----------------------------------------------------------------------------

void evaldate(unsigned char *str, struct tm *date) 
{
    date->tm_mday = evalint(str, 1);
    date->tm_mon = evalint(str + 1, 1) - 1;
    date->tm_year = evalint(str + 2, 1) + 100;
    mktime(date);
}
//-----------------------------------------------------------------------------

void evaltime(unsigned char *str, struct tm *time) 
{
    time->tm_hour = evalint(str, 1);
    time->tm_min = evalint(str + 1, 1);
    time->tm_sec = evalint(str + 2, 1);
    mktime(time);
}
//-----------------------------------------------------------------------------

void DefineECRModeDescription(void) 
{
    fr.ECRMode8Status = fr.ECRMode >> 4;
    if ((fr.ECRMode & 8) == 8) {
        fr.ECRModeDescription = strdup(ecrmode8desc[fr.ECRMode8Status]);
        return;
    };
    fr.ECRModeDescription = strdup(ecrmodedesc[fr.ECRMode]);
};

//--------------------------------------------------------------------------------------

int execute(unsigned int comm, int pass, parameter &p, answer &a)
{
    //генерируем и отсылаем комманду на устройство + повторяем попытку N раз
    int result = 0;
    int countTries = 1;
    while( countTries <= __glb_args.max_execute_tries ){
        //LOG4CXX_DEBUG( __glb_logger, "**** connect status is [" << __glb_connected << "] ****" );
        if (__glb_connected == 0) {
            //LOG4CXX_DEBUG( __glb_logger, "**** device DisConnect ****" );
            DisConnect();
            //LOG4CXX_DEBUG( __glb_logger, "**** sleep 10000 usec[0.01 sec] ****" );
            usleep(10000);
            auto_increase_timeout();            
            //LOG4CXX_DEBUG( __glb_logger, "**** device Connect ****" );
            Connect();
        }
        
        //LOG4CXX_DEBUG( __glb_logger, "**** try send CMD " << countTries << "/" << __glb_args.max_execute_tries << " tries ****" );
        if( __glb_connected == 1 ){
            p.pass = pass;
            //memcpy(p.pass, &pass, sizeof (int));
            result = sendcommand(comm, &p);
            if ( result >= 0){
                //LOG4CXX_DEBUG( __glb_logger, "**** send success ****" );
                result = readanswer(comm, &a);
                if( result >= 0 ){
                    //LOG4CXX_DEBUG( __glb_logger, "**** read success ****" ); 
                    unsigned char ch_cmd[p.cmd_len];

                    // memcpy(&ch_cmd, &comm, p.cmd_len);
                    // std::cout << ch_cmd << std::endl;
                    
    /*int sizeN = sizeof comm;
    int _sizeN = sizeN;
    int curNum = 0;
    unsigned char cmdCh[sizeN];
    std::cout << "  & ";
    for(int j=0; sizeN-->0; j++){
        if( _sizeN - j <= p.cmd_len ){
            cmdCh[j] = (comm>>(sizeN*8))&0xff;
            std::cout << j << " : " << cmdCh[j];
            cmd->buff[2 + curNum] = cmdCh[j];
            curNum ++;
        }
    }
    std::cout << std::endl;*/
                    compose_cmd(comm, p.cmd_len, ch_cmd);
                    
                    if ( memcmp(a.buff,&ch_cmd,p.cmd_len) == 0) {
                        //LOG4CXX_DEBUG( __glb_logger, "**** device return ANS for current CMD ****" );
                        result = errhand(comm, &a);
                        if( result==0 ){
                            //LOG4CXX_DEBUG( __glb_logger, "**** device return success ****" );
                        } else {
                            //LOG4CXX_ERROR( __glb_logger, "!!!! Device return ERROR !!!!" );
                        }
                        break;
                    } else {
                        result = -3;
                        //LOG4CXX_ERROR( __glb_logger, "!!!! Received ANS not for current CMD  !!!![" << result << "]" );
                    }
                } else {
                    //LOG4CXX_ERROR( __glb_logger, "!!!! Can't read ANS from device !!!![" << result << "]" );
                }
            } else {
                //LOG4CXX_ERROR( __glb_logger, "!!!! Can't send CMD to device !!!![" << result << "]" );
            }
        } else {
            result = -2;
            //LOG4CXX_ERROR( __glb_logger, "!!!! NOT __glb_connected !!!![" << result << "]" );
        }
        countTries++;
    }
    
    return result;
}

/**********************************************************
 *       Implementation of the interface functions        *
 **********************************************************/
int Connect(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ********| Connect" );
    int tries = 1;
    answer a;

    if ( __glb_connected == 1 ) {
        //LOG4CXX_DEBUG( __glb_logger, "         | allready __glb_connected try closedev" );
        closedev();
    }

    //LOG4CXX_DEBUG( __glb_logger, "         | try opendev" );
    if (opendev(&fr) == -1) {
        //LOG4CXX_ERROR( __glb_logger, "         | ! ERROR opendev" );
        __glb_connected = 0;
        return -1;
    };
    //LOG4CXX_DEBUG( __glb_logger, "         | SUCCESS opendev" );
    
    while (tries <= __glb_args.max_connect_tries ) {
        //LOG4CXX_DEBUG( __glb_logger, "         | tries from MAX  : " << std::dec << tries << " / " << std::dec << __glb_args.max_connect_tries );
        int state = checkstate();
        switch (state) {
            case NAK:
                //LOG4CXX_DEBUG( __glb_logger, "         | NAK(device await next command) 0x" << std::hex << state );
                __glb_connected = 1;
                return 1;
            case ACK:
                //LOG4CXX_DEBUG( __glb_logger, "         | ACK(device generate answer) 0x" << std::hex << state );
                //LOG4CXX_DEBUG( __glb_logger, "         | START readanswer " );
                if( readanswer(_UNKNOWN_,&a) >= 0 ){
                    if (errhand(_UNKNOWN_,&a) >= 0){

                    }
                }
                __glb_connected = 1;
                return 1;
            case -2:
                //LOG4CXX_DEBUG( __glb_logger, "         | NO CONNECT " );
            case -1:
            default:
                //LOG4CXX_DEBUG( __glb_logger, "         | UNDEFINED ["  << state << "]" );
                tries++;
        };
    };
    if(__glb_connected==1){
        // get device connect configs
        //send device connect configs
    }
    __glb_connected = 0;
    return -1;
}
//-----------------------------------------------------------------------------

int DisConnect(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ********| DisConnect" );
    //perror(" ********| DisConnect");
    if (initialized) {
        free(fr.SerialNumber);
        free(fr.License);
        free(fr.RNM);
        free(fr.INN);
        free(fr.ECRSoftVersion);
        free(fr.FMSoftVersion);
        free(fr.DataBlock);
        free(fr.ValueOfFieldString);
        free(fr.FieldName);
        free(fr.LineData);
        free(fr.LineDataExt);
        if (fr.UDescription) {
            free(fr.UDescription);
        }
        free(fr.DownloadData);
    };
    initialized = 0;
    __glb_connected = 0;
    return closedev();
}
//-----------------------------------------------------------------------------

int Beep(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Beep" );
    parameter p;
    answer a;
    int result = execute(BEEP, fr.PasswordAdmin, p, a);
    fr.OperatorNumber = a.buff[2];
    return result;
}
//-----------------------------------------------------------------------------

int CutCheck(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CutCheck");
    parameter p;
    answer a;
    p.len = 1;

    p.buff[0] = fr.CutType;
    
    int result = execute(CUT_CHECK, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    return result;
}
//-----------------------------------------------------------------------------

int PrintString(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintString");
    parameter p;
    answer a;
    p.len = 49;

    p.buff[0] = (fr.UseJournalRibbon == true) ? 1 : 0;
    p.buff[0] |= (fr.UseReceiptRibbon == true) ? 2 : 0;
    strncpy((char*) &p.buff + 1, (char*) fr.StringForPrinting, 48);

    int result = execute(PRINT_STRING, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    return result;
}
//-----------------------------------------------------------------------------

int PrintWideString(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintWideString");
    parameter p;
    answer a;
    p.len = 21;
    
    p.buff[0] = (fr.UseJournalRibbon == true) ? 1 : 0;
    p.buff[0] |= (fr.UseReceiptRibbon == true) ? 2 : 0;

    strncpy((char*) &p.buff + 1, (char*) fr.StringForPrinting, 20);

    int result = execute(PRINT_WIDE_STRING, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int FeedDocument(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD FeedDocument");
    parameter p;
    answer a;
    p.len = 2;

    p.buff[0] = (fr.UseJournalRibbon == true) ? 1 : 0;
    p.buff[0] |= (fr.UseReceiptRibbon == true) ? 2 : 0;
    p.buff[0] |= (fr.UseSlipDocument == true) ? 4 : 0;
    
    if( fr.StringQuantity <= 0 || fr.StringQuantity > 255 ){
        fr.StringQuantity = 1;
    }
    p.buff[1] = fr.StringQuantity;

    int result = execute(FEED_DOCUMENT, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    return result;
}
//-----------------------------------------------------------------------------

int SetExchangeParam(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD SetExchangeParam");
    parameter p;
    answer a;
    p.len = 3;

    p.buff[0] = fr.PortNumber;
    p.buff[1] = fr.BaudRate;
    p.buff[2] = fr.Timeout;

    int result = execute(SET_EXCHANGE_PARAM, fr.Password, p, a);
    return result;
}
//-----------------------------------------------------------------------------

int GetExchangeParam(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetExchangeParam");
    parameter p;
    answer a;
    p.len = 1;

    p.buff[0] = fr.PortNumber;

    int result = execute(GET_EXCHANGE_PARAM, fr.Password, p, a);
    fr.BaudRate = a.buff[2];
    fr.Timeout = a.buff[3];
    return result;

}
//-----------------------------------------------------------------------------

int GetShortStatus(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetShortStatus");
    int len = 0;
    parameter p;
    answer a;

    /* Ответ: 10H. Длина сообщения: 16 или 17 1 байт.
     * Код ошибки (1 байт)
     */
    int result = execute(GET_SHORT_STATUS, fr.Password, p, a);
    /* Порядковый номер оператора (1 байт) 1...30 */
    fr.OperatorNumber = a.buff[2];
    /* Флаги ККТ (2 байта)
     * Битовое поле (назначение бит):
     *  0 – Рулон операционного журнала (контрольной ленты) (0 – нет, 1 – есть)
     *  1 – Рулон чековой ленты (0 – нет, 1 – есть)
     *  2 – Верхний датчик подкладного документа (0 – нет, 1 – да)
     *  3 – Нижний датчик подкладного документа (0 – нет, 1 – да)
     *  4 – Положение десятичной точки (0 – 0 знаков, 1 – 2 знака)
     *  5 – ЭКЛЗ 2 (0 – нет, 1 – есть)
     *  6 – Оптический датчик операционного журнала (контрольной ленты) (0 – бумаги нет, 1 – бумага есть)
     *  7 – Оптический датчик чековой ленты (0 – бумаги нет, 1 – бумага есть)
     *  8 – Рычаг термоголовки контрольной ленты (0 – поднят, 1 – опущен)
     *  9 – Рычаг термоголовки чековой ленты (0 – поднят, 1 – опущен)
     *  10 – Крышка корпуса ККТ (0 – опущена, 1 – поднята)
     *  11 – Денежный ящик (0 – закрыт, 1 – окрыт)
     *  12а – Отказ правого датчика принтера (0 – нет, 1 – да)
     *  12б – Бумага на входе в презентер (0 – нет, 1 – да)
     *  12в – Модель принтера (0 – MLT-286, 1 – модель MLT-286-1)
     *  12г – Крышка корпуса ККТ контрольной ленты (0 – опущена, 1 – поднята)
     *  13а – Отказ левого датчика принтера (0 – нет, 1 – да)
     *  13б – Бумага на выходе из презентера (0 – нет, 1 – да)
     *  14 – ЭКЛЗ 2 почти заполнена (0 – нет, 1 – да) 2
     *  15а – Увеличенная точность количества (0 – нормальная точность, 1 – увеличенная точность) [для ККМ без ЭКЛЗ 2 ]. Для ККМ с ЭКЛЗ (1 – нормальная точность, 0 – увеличенная точность)
     *  15б – Буфер принтера непуст (0 – пуст, 1 – непуст) 
     *  Актуальность флагов ККТ:
     *      1 (0x0002) Рулон чековой ленты
     *      4 (0x0010) Положение десятичной точки
     *      7 (0x0080) Оптический датчик чековой ленты
     *      9 (0x0200) Рычаг термоголовки чековой ленты
     *      10 (0x0400) Крышка корпуса ФР
     *      11 (0x0800) Бумага в презенторе
     */
    fr.ECRFlags = evalint((unsigned char*) &a.buff + 3, 2);
    /* Режим ККТ (1 байт)
     * 1. Выдача данных.
     * 2. Открытая смена, 24 часа не кончились.
     * 3. Открытая смена, 24 часа кончились.
     * 4. Закрытая смена.
     * 5. Ожидание подтверждения ввода даты.
     * 6. Открытый документ:
     *      8.0. Приход.
     *      8.1. Расход.
     *      8.2. Возврат прихода.
     *      8.3. Возврат расхода.
     * 9. Режим разрешения технологического обнуления. В этот режим ККМ переходит по включению питания, если некорректна информация в энергонезависимом ОЗУ ККМ.
     * 10. Тестовый прогон.
     */
    fr.ECRMode = a.buff[5];
    DefineECRModeDescription();
    /* Подрежим ККТ (1 байт)
     * 0. Бумага есть – ККТ не в фазе печати операции – может принимать от хоста команды, связанные с печатью на том документе, датчик которого сообщает о наличии бумаги.
     * 1. Пассивное отсутствие бумаги – ККТ не в фазе печати операции – не принимает от хоста команды, связанные с печатью на том документе, датчик которого сообщает об отсутствии бумаги.
     * 2. Активное отсутствие бумаги – ККТ в фазе печати операции – принимает только команды, не связанные с печатью. Переход из этого подрежима только в подрежим 3.
     * 3. После активного отсутствия бумаги – ККТ ждет команду продолжения печати. Кроме этого принимает команды, не связанные с печатью.
     * 4. Фаза печати операции полных фискальных отчетов 1 – ККТ не принимает от хоста команды, связанные с печатью, кроме команды прерывания печати.
     * 5. Фаза печати операции – ККТ не принимает от хоста команды, связанные с печатью.
     */
    fr.ECRSubmode = a.buff[6];
    fr.ECRSubmodeDescription = get_submodedesc_message(fr.ECRSubmode);
    /* Количество операций в чеке (1 байт) младший байт двухбайтного числа (см. ниже) */
    fr.ECRCountOperationInReceiptLow = a.buff[7];
    /* Напряжение резервной батареи (1 байт) */
    fr.ECRVoltageOfReservBattary = a.buff[8];
    /* Напряжение источника питания (1 байт) */
    fr.ECRVoltageOfSource = a.buff[9];
    /* Код ошибки ФП 2 (1 байт) */
    fr.ECRErrorCodeFP2 = a.buff[10];
    /* Код ошибки ЭКЛЗ 2 (1 байт) */
    fr.ECRErrorCodeEKLZ2 = a.buff[11];
    /* Количество операций в чеке (1 байт) старший байт двухбайтного числа (см. выше) */
    fr.ECRCountOperationInReceiptHight = a.buff[12];
    /* Зарезервировано (3 байта) */
    //fr. = evalint((unsigned char*) &a.buff + 13, 3);
    
    /* Результат последней печати 1 (1 байт) 
     * Причина завершения печати или промотки бумаги:
     * 0 – печать завершена успешн
     * 1 – произошел обрыв бумаги
     * 2 – ошибка принтера (перегрев головки, другая ошибка)
     * 5 – идет печать
     */
    fr.ECRLastPrintResult = a.buff[16];
    
    return result;
}

int GetECRStatus(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetECRStatus");
    parameter p;
    answer a;

    int result = execute(GET_ECR_STATUS, fr.Password, p, a);

    // Порядковый номер оператора (1 байт) 1...30
    fr.OperatorNumber = a.buff[2];

    // Версия ПО ККТ (2 байта)
    fr.ECRSoftVersion[0] = a.buff[3];
    fr.ECRSoftVersion[1] = 0x2e;
    fr.ECRSoftVersion[2] = a.buff[4];
    fr.ECRSoftVersion[3] = 0;

    // Сборка ПО ККТ (2 байта)
    fr.ECRBuild = evalint((unsigned char*) &a.buff + 5, 2);
    // Дата ПО ККТ (3 байта) ДД-ММ-ГГ
    evaldate((unsigned char*) &a.buff + 7, &fr.ECRSoftDate);
    // Номер в зале (1 байт)
    fr.LogicalNumber = evalint((unsigned char*) &a.buff + 10, 1);
    // Сквозной номер текущего документа (2 байта)
    fr.OpenDocumentNumber = evalint((unsigned char*) &a.buff + 11, 2);
    // Флаги ККТ (2 байта)
    fr.ReceiptRibbonIsPresent = (a.buff[13] & 1) == 1; //0
    fr.JournalRibbonIsPresent = (a.buff[13] & 2) == 2; //1
    fr.SlipDocumentIsPresent = (a.buff[13] & 4) == 4; //2
    fr.SlipDocumentIsMoving = (a.buff[13] & 8) == 8; //3
    fr.PointPosition = (a.buff[13] & 16) == 16; //4
    fr.EKLZIsPresent = (a.buff[13] & 32) == 32; //5
    fr.JournalRibbonOpticalSensor = (a.buff[13] & 64) == 64; //6
    fr.ReceiptRibbonOpticalSensor = (a.buff[13] & 128) == 128; //6
    fr.JournalRibbonLever = (a.buff[14] & 1) == 1; //0
    fr.ReceiptRibbonLever = (a.buff[14] & 2) == 2; //1
    fr.LidPositionSensor = (a.buff[14] & 4) == 4; //2

    // Режим ККТ (1 байт)
    fr.ECRMode = evalint((unsigned char*) &a.buff + 15, 1);
    DefineECRModeDescription();

    // Подрежим ККТ (1 байт)
    fr.ECRAdvancedMode = evalint((unsigned char*) &a.buff + 16, 1);
    fr.ECRAdvancedModeDescription = strdup(ecrsubmodedesc[fr.ECRAdvancedMode]);

    // Порт ККТ
    fr.PortNumber = evalint((unsigned char*) &a.buff + 17, 1);
    // Версия ПО ФП (2 байта)
    fr.FMSoftVersion[0] = a.buff[18];
    fr.FMSoftVersion[1] = 0x2e;
    fr.FMSoftVersion[2] = a.buff[19];
    fr.FMSoftVersion[3] = 0;
    // Сборка ПО ФП (2 байта)
    fr.FMBuild = evalint((unsigned char*) &a.buff + 20, 2);
    // Дата ПО ФП (3 байта) ДД-ММ-ГГ
    evaldate((unsigned char*) &a.buff + 22, &fr.FMSoftDate);
    // Дата (3 байта) ДД-ММ-ГГ
    evaldate((unsigned char*) &a.buff + 25, &fr.Date);
    // Время (3 байта) ЧЧ-ММ-СС
    evaltime((unsigned char*) &a.buff + 28, &fr.Time);
    
    // Флаги ФП (1 байт) младший байт двухбайтного числа (см. ниже)
    fr.FM1IsPresent = (a.buff[31] & 1) == 1;
    fr.FM2IsPresent = (a.buff[31] & 2) == 2;
    fr.LicenseIsPresent = (a.buff[31] & 4) == 4;
    fr.FMOverflow = (a.buff[31] & 8) == 8;
    fr.BatteryCondition = (a.buff[31] & 16) == 16;
    // Заводской номер (4 байта) младшее длинное слово 6-байтного числа (см. ниже)
    sprintf(fr.SerialNumber, "%d", evalint((unsigned char*) &a.buff + 32, 4));
    
    // Номер последней закрытой смены (2 байта)
    fr.SessionNumber = evalint((unsigned char*) &a.buff + 36, 2);
    // Количество свободных записей в ФП (2 байта)
    fr.FreeRecordInFM = evalint((unsigned char*) &a.buff + 38, 2);
    // Количество перерегистраций (фискализаций) (1 байт)
    fr.RegistrationNumber = evalint((unsigned char*) &a.buff + 40, 1);
    // Количество оставшихся перерегистраций (фискализаций) (1 байт)
    fr.FreeRegistration = evalint((unsigned char*) &a.buff + 41, 1);
    // ИНН (6 байт)
    sprintf(fr.INN, "%.0lg", (double) evalint64((unsigned char*) &a.buff + 42, 6));
    
    //     Флаги ФП (1 байт) старший байт двухбайтного числа
    // Режим ФП (1 байт)
    // Заводской номер (2 байта) старшее слово 6-байтного числа

/*
Ответ: 11H. Длина сообщения: 48.

    Версия ПО ККТ   |   2 WIN1251-символа, между которыми надо вставить символ «точка». Например, «10» соответствует 1.0
    Сборка ПО ККТ   |   0...65535
    Дата ПО ККТ     |   Дата выпуска программного обеспечения системной платы ДД-ММ-ГГ
    Номер в зале    |   01...99
    Сквозной номер
    текущего    
    документа       |   0000...9999
    Флаги ККТ       |   Битовое поле (назначение бит):
                        0 – Рулон операционного журнала (контрольной ленты) (0 – нет, 1 – есть)
                        1 – Рулон чековой ленты (0 – нет, 1 – есть)
                        2 – Верхний датчик подкладного документа (0 – нет, 1 – да)
                        3 – Нижний датчик подкладного документа (0 – нет, 1 – да)
                        4 – Положение десятичной точки (0 – 0 знаков, 1 – 2 знака)
                        5 – ЭКЛЗ 6 (0 – нет, 1 – есть)
                        6 – Оптический датчик операционного журнала (контрольной ленты) (0 –
                        бумаги нет, 1 – бумага есть)
                        7 – Оптический датчик чековой ленты (0 – бумаги нет, 1 – бумага есть)
                        8 – Рычаг термоголовки контрольной ленты (0 – поднят, 1 – опущен)
                        9 – Рычаг термоголовки чековой ленты (0 – поднят, 1 – опущен)
                        10 – Крышка корпуса ККТ (0 – опущена, 1 – поднята)
                        11 – Денежный ящик (0 – закрыт, 1 – окрыт)
                        12а – Отказ правого датчика принтера (0 – нет, 1 – да)
                        12б – Бумага на входе в презентер (0 – нет, 1 – да)
                        12в – Модель принтера (0 – MLT-286, 1 – модель MLT-286-1)
                        12г – Крышка корпуса ККТ контрольной ленты (0 – опущена, 1 – поднята)
                        13а – Отказ левого датчика принтера (0 – нет, 1 – да)
                        13б – Бумага на выходе из презентера (0 – нет, 1 – да)
                        14 – ЭКЛЗ 6 почти заполнена (0 – нет, 1 – да)
                        15а – Увеличенная точность количества (0 – нормальная точность, 1 –
                        увеличенная точность) [для ККМ без ЭКЛЗ 6 ]
                        15б – Буфер принтера непуст (0 – пуст, 1 – непуст)
                        Актуальность флагов ККТ:
                            1 (0x0002) Рулон чековой ленты
                            4 (0x0010) Положение десятичной точки
                            7 (0x0080) Оптический датчик чековой ленты
                            9 (0x0200) Рычаг термоголовки чековой ленты
                            10 (0x0400) Крышка корпуса ФР
                            11 (0x0800) Бумага в презенторе
    Режим ККТ       |   одно из состояний ККМ, в котором она может находиться. Режимы
                        ККМ описываются одним байтом: младший полубайт – номер режима, старший полубайт –
                        битовое поле, определяющее статус режима (для режимов 8). Номера и назначение режимов
                        и статусов:
                        1. Выдача данных.
                        2. Открытая смена, 24 часа не кончились.
                        3. Открытая смена, 24 часа кончились.
                        4. Закрытая смена.
                        5. Ожидание подтверждения ввода даты.
                        6. Открытый документ:
                            8.0. Приход.
                            8.1. Расход.
                            8.2. Возврат прихода.
                            8.3. Возврат расхода.
                        9. Режим разрешения технологического обнуления. В этот режим ККМ переходит по
                            включению питания, если некорректна информация в энергонезависимом ОЗУ
                            ККМ.
                        10. Тестовый прогон.

    Подрежим ККТ    |   одно из состояний ККТ , в котором он может находиться. Номера и назначение подрежимов:
                        0. Бумага есть – ККТ не в фазе печати операции – может принимать от хоста команды,
                        связанные с печатью на том документе, датчик которого сообщает о наличии бумаги.
                        1. Пассивное отсутствие бумаги – ККТ не в фазе печати операции – не принимает от
                        хоста команды, связанные с печатью на том документе, датчик которого сообщает об
                        отсутствии бумаги.
                        2. Активное отсутствие бумаги – ККТ в фазе печати операции – принимает только
                        команды, не связанные с печатью. Переход из этого подрежима только в подрежим 3.
                        3. После активного отсутствия бумаги – ККТ ждет команду продолжения печати. Кроме
                        этого принимает команды, не связанные с печатью.
                        4. Фаза печати операции полных фискальных отчетов 1 – ККТ не принимает от хоста
                        команды, связанные с печатью, кроме команды прерывания печати.
                        5. Фаза печати операции – ККТ не принимает от хоста команды, связанные с печатью.
    Порт            |   Номер порта ККТ, к которому подключен хост. 
                        Формат – двоичное число из диапазона: 
                            0...127 – COM-порты; 
                            128 – TCP сокет; 
                            129...255 – зарезервировано
    Версия ПО ФП    |   2 WIN1251-символа, между которыми надо вставить символ «точка». Например «10» соответствует 1.0
    Сборка ПО ФП    |   0...65535
    Дата ПО ФП      |   Дата выпуска программного обеспечения фискальной памяти ДД-ММ-ГГ
    Дата ДД-ММ-ГГ   |   Дата ДД-ММ-ГГ
    Время ЧЧ-ММ-СС  |   00...23, 00...59, 00...59 – показания внутренних часов ККМ
    Флаги ФП        |   Битовое поле (назначение бит):
                            0 – ФП 1 / Накопитель ФП 1 (0 – нет, 1 – есть)
                            1 – ФП 2 / Оперативная память (файл) ФП 1 (0 – нет, 1 – есть)
                            2 – Лицензия / Заводской номер 1 (0 – не введена, 1 – введена)
                            3 – Переполнение ФП (0 – нет, 1 – есть)
                            4 – Батарея ФП (0 – >80%, 1 – <80%)
                            5 – Последняя запись ФП (0 – испорчена, 1 – корректна)
                            6 – Смена в ФП (0 – закрыта, 1 – открыта)
                            7 – 24 часа в ФП (0 – не кончились, 1 – кончились)
    Зав. Номер      |   00000000...99999999 (FFh FFh FFh FFh – заводской номер не введен)
    Номер последней
    закрытой смены  |   0000...9999
    Количество
    свободных 
    записей в ФП    |   0000...9999
    Количество
    перерегистраций
    (фискализаций)  |   0...20(максимальная длина строки зависит от длины сообщения;)
    Количество
    оставшихся
    перерегистраций
    (фискализаций)  |   0...20(максимальная длина строки зависит от длины сообщения;)
    ИНН             |   00000000000000...99999999999999 (FFh FFh FFh FFh FFh FFh – ИНН не введен) ≤14 знаков
                            (максимальная длина строки зависит от длины сообщения;)

    Флаги ФП 
    (старший байт)  |   Битовое поле (назначение бит):
                            9 – АСПД (0 – нет, есть записи активизации ЭКЛЗ в ФП, 1 – да)
                            10 – Блокировка ККТ по неверному паролю НИ (0 – нет, 1 – есть)
                            11 – Зарезервировано
                            12 – Три или более поврежденных записей сменных итогов в ФП (0 – нет, 1 – да)
                            13 – Запись фискализации или активизации ЭКЛЗ или заводского номера в накопителе повреждена (0 – нет, 1 – да)
                            14 – Зарезервировано
                            15 – Последняя запись в накопителе ФП (0 – фискализации/активизации ЭКЛЗ, 1 – сменного итога)
    Режим ФП        |   1 – Выдача данных оперативной памяти ФП
                        2 – Выдача данных накопителя ФП
                        3 – Выдача данных полного фискального отчета
                        4 – Нормальное состояние ФП
                        5 – Выдача данных памяти программ ФП
                        9 – Начальная инициализация ОЗУ ФП (тех. обнуление)
    Зав. Номер 
    (старшее слово) |   0000...9999 (FFh FFh – заводской номер не введен)
 */

    return result;
}
//-----------------------------------------------------------------------------

int GetDeviceMetrics(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetDeviceMetrics");
    int len = 0;
    parameter p;
    answer a;

    /* Ответ: FCH. Длина сообщения: (8+X) байт.
     * Код ошибки (1 байт)
     */
    int result = execute(GET_DEVICE_METRICS, fr.Password, p, a);
    /* Устройство          | PAYONLINE-01-ФА
     * Возвращаемое 
     * название устройства | PAYONLINE-01-ФА
     * Версия протокола    | 2
     * Подверсия протокола | 14
     * Модель устройства   | 255
     * Язык устройства     | 0
     * Подтип устройства   | 0
     * Тип устройства      | ККТ
     */ 
    len = a.len - 7;
    /* Тип устройства (1 байт) 0...255 */
    fr.UMajorType = evalint((unsigned char*) &a.buff + 2, 1);
    /* Подтип устройства (1 байт) 0...255 */
    fr.UMinorType = evalint((unsigned char*) &a.buff + 3, 1);
    /* Версия протокола для данного устройства (1 байт) 0...255 */
    fr.UMajorProtocolVersion = evalint((unsigned char*) &a.buff + 4, 1);
    /* Подверсия протокола для данного устройства (1 байт) 0...255 */
    fr.UMinorProtocolVersion = evalint((unsigned char*) &a.buff + 5, 1);
    /* Модель устройства (1 байт) 0...255 */
    fr.UModel = evalint((unsigned char*) &a.buff + 6, 1);
    /* Язык устройства (1 байт): 
     *  «0» – русский;
     *  «1» – английский;
     *  «2» – эстонский;
     *  «3» – казахский;
     *  «4» – белорусский;
     *  «5» – армянский;
     *  «6» – грузинский;
     *  «7» – украинский;
     *  «8» – киргизский;
     *  «9» – туркменский;
     *  «10» – молдавский;
     */
    fr.UCodePage = evalint((unsigned char*) &a.buff + 7, 1);
    /* Название устройства – строка символов в кодировке WIN1251. Количество байт, отводимое под название устройства, определяется в каждом конкретном случае самостоятельно разработчиками устройства (X байт) */
    fr.UDescription = (char*) malloc(len + 1);
    strncpy(fr.UDescription, (char*) a.buff + 8, len);

    return result;
}
//-----------------------------------------------------------------------------

int Test(void) {
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Test");
    parameter p;
    answer a;
    p.len = 1;

    p.buff[0] = fr.RunningPeriod;
    
    int result = execute(TEST, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int InterruptTest(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD InterruptTest");
    parameter p;
    answer a;
    
    int result = execute(INTERRUPT_TEST, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

/*
 * Продолжение печати
 */
int ContinuePrinting(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ContinuePrinting");
    parameter p;
    answer a;

    /*
     * Команда: B0H. Длина сообщения: 5 байт.
     * Пароль оператора, администратора или системного администратора (4 байта)
     */
    int result = execute(CONTINUE_PRINTING, fr.PasswordAdmin, p, a);

    /*
     * Ответ: B0H. Длина сообщения: 3 байта.
     * Код ошибки (1 байт)
     */

    /* Порядковый номер оператора (1 байт) 1...30 */
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int OpenDrawer(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD OpenDrawer");
    parameter p;
    answer a;
    p.len = 1;

    p.buff[0] = fr.DrawerNumber;

    int result = execute(OPEN_DRAWER, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int PrintDocumentTitle(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintDocumentTitle");
    parameter p;
    answer a;
    p.len = 32;

    strncpy((char*) p.buff, (char*) fr.DocumentName, 30);
    memcpy(p.buff + 30, &fr.DocumentNumber, 2);
    
    int result = execute(PRINT_DOCUMENT_TITLE, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int ResetSettings(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ResetSettings");
    answer a;
    parameter p;

    int result = execute(RESET_SETTINGS, fr.Password, p, a);

    return result;
}
//-----------------------------------------------------------------------------

int ResetSummary(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ResetSummary");
    parameter p;
    answer a;
    
    int result = execute(RESET_SUMMARY, fr.Password, p, a);

    return result;
}
//-----------------------------------------------------------------------------

int Buy(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Buy");

    parameter p;
    answer a;
    p.len = 55;

    int64_t quant = llround(fr.Quantity * 1000);
    int64_t price = llround(fr.Price * 100);

    memcpy(p.buff, &quant, 5);
    memcpy(p.buff + 5, &price, 5);
    p.buff[10] = fr.Department;

    p.buff[11] = fr.Tax1;
    p.buff[12] = fr.Tax2;
    p.buff[13] = fr.Tax3;
    p.buff[14] = fr.Tax4;

    strncpy((char*) p.buff + 15, (char*) fr.StringForPrinting, 40);

    int result = execute(BUY, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int ReturnBuy(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ReturnBuy");

    parameter p;
    answer a;
    p.len = 55;

    int64_t quant = llround(fr.Quantity * 1000);
    int64_t price = llround(fr.Price * 100);

    memcpy(p.buff, &quant, 5);
    memcpy(p.buff + 5, &price, 5);
    p.buff[10] = fr.Department;

    p.buff[11] = fr.Tax1;
    p.buff[12] = fr.Tax2;
    p.buff[13] = fr.Tax3;
    p.buff[14] = fr.Tax4;

    strncpy((char*) p.buff + 15, (char*) fr.StringForPrinting, 40);

    int result = execute(RETURN_BUY, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    
    return 0;
}
//-----------------------------------------------------------------------------

int Sale(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Sale");
    printf(" ||-------> CMD Sale\n");
    parameter p;
    answer a;
    p.len = 55;

    int64_t quant = llround(fr.Quantity * 1000);
    int64_t price = llround(fr.Price * 100);

    memcpy(p.buff, &quant, 5);
    memcpy(p.buff + 5, &price, 5);
    p.buff[10] = fr.Department;

    p.buff[11] = fr.Tax1;
    p.buff[12] = fr.Tax2;
    p.buff[13] = fr.Tax3;
    p.buff[14] = fr.Tax4;

    strncpy((char*) p.buff + 15, (char*) fr.StringForPrinting, 40);
    
    int result = execute(SALE, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int ReturnSale(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ReturnSale");

    parameter p;
    answer a;
    p.len = 55;

    int64_t quant = llround(fr.Quantity * 1000);
    int64_t price = llround(fr.Price * 100);

    memcpy(p.buff, &quant, 5);
    memcpy(p.buff + 5, &price, 5);
    p.buff[10] = fr.Department;

    p.buff[11] = fr.Tax1;
    p.buff[12] = fr.Tax2;
    p.buff[13] = fr.Tax3;
    p.buff[14] = fr.Tax4;

    strncpy((char*) p.buff + 15, (char*) fr.StringForPrinting, 40);

    int result = execute(RETURN_SALE, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int CancelCheck(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CancelCheck");
    parameter p;
    answer a;

    int result = execute(CANCEL_CHECK, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int CashIncome(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CashIncome");
    parameter p;
    answer a;
    p.len = 5;

    int64_t sum = llround(fr.Summ1 * 100);

    memcpy(p.buff, &sum, 5);

    int result = execute(CASH_INCOME, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.OpenDocumentNumber = evalint((unsigned char*) &a.buff + 3, 2);

    return result;

}
//-----------------------------------------------------------------------------

int CashOutcome(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CashOutcome");

    parameter p;
    answer a;
    p.len = 5;

    int64_t sum = llround(fr.Summ1 * 100);

    memcpy(p.buff, &sum, 5);

    int result = execute(CASH_OUTCOME, fr.Password, p, a);
    
    fr.OperatorNumber = a.buff[2];
    fr.OpenDocumentNumber = evalint((unsigned char*) &a.buff + 3, 2);

    return result;
}
//-----------------------------------------------------------------------------

int Charge(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Charge");

    parameter p;
    answer a;
    p.len = 49;

    int64_t sum = llround(fr.Summ1 * 100);

    memcpy(p.buff, &sum, 5);

    p.buff[5] = fr.Tax1;
    p.buff[6] = fr.Tax2;
    p.buff[7] = fr.Tax3;
    p.buff[8] = fr.Tax4;

    strncpy((char*) p.buff + 9, (char*) fr.StringForPrinting, 40);

    int result = execute(CHARGE, fr.Password, p, a);

    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int StornoCharge(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD StornoCharge");

    parameter p;
    answer a;
    p.len = 49;

    int64_t sum = llround(fr.Summ1 * 100);

    memcpy(p.buff, &sum, 5);

    p.buff[5] = fr.Tax1;
    p.buff[6] = fr.Tax2;
    p.buff[7] = fr.Tax3;
    p.buff[8] = fr.Tax4;

    strncpy((char*) p.buff + 9, (char*) fr.StringForPrinting, 40);

    int result = execute(STORNO_CHARGE, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int Discount(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Discount");
    parameter p;
    answer a;
    p.len = 49;

    int64_t sum = llround(fr.Summ1 * 100);

    memcpy(p.buff, &sum, 5);

    p.buff[5] = fr.Tax1;
    p.buff[6] = fr.Tax2;
    p.buff[7] = fr.Tax3;
    p.buff[8] = fr.Tax4;

    strncpy((char*) p.buff + 9, (char*) fr.StringForPrinting, 40);

    int result = execute(DISCOUNT, fr.Password, p, a);    
    fr.OperatorNumber = a.buff[2];

    return 0;
}
//-----------------------------------------------------------------------------

int StornoDiscount(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD StornoDiscount");
    parameter p;
    answer a;
    p.len = 49;

    int64_t sum = llround(fr.Summ1 * 100);

    memcpy(p.buff, &sum, 5);

    p.buff[5] = fr.Tax1;
    p.buff[6] = fr.Tax2;
    p.buff[7] = fr.Tax3;
    p.buff[8] = fr.Tax4;

    strncpy((char*) p.buff + 9, (char*) fr.StringForPrinting, 40);
    
    int result = execute(STORNO_DISCOUNT, fr.Password, p, a);    
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int CheckSubTotal(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CheckSubTotal");
    printf(" ||-------> CMD CheckSubTotal\n");
    parameter p;
    answer a;

    int result = execute(CHECK_SUBTOTAL, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.Summ1 = evalint64((unsigned char*) &a.buff + 3, 5);
    fr.Summ1 /= 100;

    return result;
}
//-----------------------------------------------------------------------------

int OpenCheck(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD OpenCheck");
    printf(" ||-------> CMD OpenCheck\n");
    parameter p;
    answer a;

    p.len = 1;
/*
Тип документа (1 байт):
«0» – продажа
«1» – покупка
«2» – возврат продажи
«3» – возврат покупки
 */
    p.buff[0] = fr.CheckType;

    int result = execute(OPEN_CHECK, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    
    return result;
}

int CloseCheck(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CloseCheck");
    printf(" ||-------> CMD CloseCheck\n");
    parameter p;
    answer a;

    int len = 0;
    int64_t sum;
    p.len = 67;

    sum = llround(fr.Summ1 * 100);
    memcpy(p.buff, &sum, 5); // 0-4
    sum = llround(fr.Summ2 * 100);
    memcpy(p.buff + 5, &sum, 5); // 5-9
    sum = llround(fr.Summ3 * 100);
    memcpy(p.buff + 10, &sum, 5); //10-14
    sum = llround(fr.Summ4 * 100);
    memcpy(p.buff + 15, &sum, 5); //15-19

    sum = llround(fr.DiscountOnCheck * 100);
    memcpy(p.buff + 20, &sum, 3); //20-22

    p.buff[23] = fr.Tax1; //23
    p.buff[24] = fr.Tax2; //24
    p.buff[25] = fr.Tax3; //25
    p.buff[26] = fr.Tax4; //26

    strncpy((char*) p.buff + 27, (char*) fr.StringForPrinting, 40);
    
    int result = execute(CLOSE_CHECK, fr.Password, p, a);    
    fr.OperatorNumber = a.buff[2];
    fr.Change = evalint64((unsigned char*) &a.buff + 3, 5);
    fr.Change /= 100;

    len = a.len;

    if (len > 8) {
        fr.Link = (char*) malloc(247);
        memset(fr.Link, 0, 247);
        memcpy(fr.Link, (char*) a.buff + 7, len);
    }

    return result;
}
//-----------------------------------------------------------------------------

int Storno(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Storno");

    parameter p;
    answer a;

    int64_t quant = llround(fr.Quantity * 1000);
    int64_t price = llround(fr.Price * 100);
    p.len = 55;

    memcpy(p.buff, &quant, 5);
    memcpy(p.buff + 5, &price, 5);
    p.buff[10] = fr.Department;

    p.buff[11] = fr.Tax1;
    p.buff[12] = fr.Tax2;
    p.buff[13] = fr.Tax3;
    p.buff[14] = fr.Tax4;

    strncpy((char*) p.buff + 15, (char*) fr.StringForPrinting, 40);

    int result = execute(STORNO, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int PrintReportWithCleaning(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintReportWithCleaning");
    parameter p;
    answer a;

    int result = execute(PRINT_REPORT_WITH_CLEANING, fr.PasswordAdmin, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.SessionNumber = evalint((unsigned char*) &a.buff + 3, 2);

    return result;
}
//-----------------------------------------------------------------------------

int PrintReportWithoutCleaning(void)
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintReportWithoutCleaning");

    parameter p;
    answer a;

    int result = execute(PRINT_REPORT_WITHOUT_CLEANING, fr.PasswordAdmin, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.SessionNumber = evalint((unsigned char*) &a.buff + 3, 2);

    return result;
}
//-----------------------------------------------------------------------------
/**
Суточный отчет с гашением в буфер
Команда:
C6H. Длина сообщения: 5 байт.
Пароль оператора (4 байта)
Ответ:
C6H. Длина сообщения: 3 байта.
Код ошибки (1 байт)
Порядковый номер оператора (1 байт) 1...30
 * @return 
 */
int PrintReportWithCleaningToBuffer(void)
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintReportWithCleaningToBuffer");

    parameter p;
    answer a;

    int result = execute(PRINT_REPORT_WITH_CLEANING_TO_BUFFER, fr.PasswordAdmin, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.SessionNumber = evalint((unsigned char*) &a.buff + 3, 2);

    return result;
}
//-----------------------------------------------------------------------------

int PrintOperationReg(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintOperationReg");
    parameter p;
    answer a;

    int result = execute(PRINT_OPERATION_REG, fr.PasswordAdmin, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int DampRequest(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD DampRequest");

    parameter p;
    answer a;

    p.len = 1;
    p.buff[0] = fr.DeviceCode;

    int result = execute(DUMP_REQUEST, fr.PasswordAdmin, p, a);
    fr.DataBlockNumber = a.buff[2];//number of data blocks
    
    return result; 
}
//-----------------------------------------------------------------------------

int GetData(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetData");

    parameter p;
    answer a;

    int result = execute(GET_DATA, fr.PasswordAdmin, p, a);
    
    fr.DeviceCode = a.buff[2];
    fr.DeviceCodeDescription = strdup(devcodedesc[fr.DeviceCode]);
    fr.DataBlockNumber = evalint((unsigned char*) &a.buff + 3, 2);
    memcpy(fr.DataBlock, a.buff + 5, 32);

    return result;
}
//-----------------------------------------------------------------------------

int InterruptDataStream(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD InterruptDataStream");

    parameter p;
    answer a;

    int result = execute(INTERRUPT_DATA_STREAM, fr.PasswordAdmin, p, a);
    return result;
}
//-----------------------------------------------------------------------------

int GetCashReg(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetCashReg");

    parameter p;
    answer a;

    p.len = 1;

    p.buff[0] = fr.RegisterNumber;

    int result = execute(GET_CASH_REG, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.ContentsOfCashRegister = evalint64((unsigned char*) &a.buff + 3, 6);
    fr.ContentsOfCashRegister /= 100;

    return result;
}
//-----------------------------------------------------------------------------

int GetOperationReg(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetOperationReg");

    parameter p;
    answer a;

    p.len = 1;

    p.buff[0] = fr.RegisterNumber;

    int result = execute(GET_OPERATION_REG, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    fr.ContentsOfOperationRegister = evalint((unsigned char*) &a.buff + 3, 2);

    return result;
}
//-----------------------------------------------------------------------------

int SetSerialNumber(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD SetSerialNumber");
    parameter p;
    answer a;

    p.len = 4;
    int num = atol(fr.SerialNumber);

    memcpy(p.buff, &num, sizeof (int));

    int result = execute(SET_SERIAL_NUMBER, fr.Password, p, a);
    return result;
}
//-----------------------------------------------------------------------------

int SetPointPosition(void) {
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD SetPointPosition");
    parameter p;
    answer a;

    p.len = 1;
    p.buff[0] = fr.PointPosition;

    int result = execute(SET_POINT_POSITION, fr.Password, p, a);

    return result;
}
//-----------------------------------------------------------------------------
/**
Программирование времени
Команда:
21H. Длина сообщения: 8 байт.
Пароль системного администратора (4 байта)
Время (3 байта) ЧЧ-ММ-СС
Ответ:
21H. Длина сообщения: 2 байта.
Код ошибки (1 байт)
 * @return 
 */
int SetTime(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD SetTime");
    parameter p;
    answer a;

    p.len = 3;

    p.buff[2] = fr.Time.tm_sec;
    p.buff[1] = fr.Time.tm_min;
    p.buff[0] = fr.Time.tm_hour;

    int result = execute(SET_TIME, fr.PasswordAdmin, p, a);

    return result;
}
//-----------------------------------------------------------------------------
/**
Программирование даты
Команда:
22H. Длина сообщения: 8 байт.
Пароль системного администратора (4 байта)
Дата (3 байта) ДД-ММ-ГГ
Ответ:
22H. Длина сообщения: 2 байта.
Код ошибки (1 байт)
 * @return 
 */
int SetDate(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD SetDate");

    parameter p;
    answer a;

    p.len = 3;

    p.buff[0] = fr.Date.tm_mday;
    p.buff[1] = fr.Date.tm_mon + 1;
    p.buff[2] = fr.Date.tm_year - 100;
    
    int result = execute(SET_DATE, fr.PasswordAdmin, p, a);
    return result;
}
//-----------------------------------------------------------------------------
/**
Подтверждение программирования даты
Команда:
23H. Длина сообщения: 8 байт.
Пароль системного администратора (4 байта)
Дата (3 байта) ДД-ММ-ГГ
Ответ:
23H. Длина сообщения: 2 байта.
Код ошибки (1 байт)
 * @return 
 */
int ConfirmDate(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ConfirmDate");
    parameter p;
    answer a;

    p.len = 3;

    p.buff[0] = fr.Date.tm_mday;
    p.buff[1] = fr.Date.tm_mon + 1;
    p.buff[2] = fr.Date.tm_year - 100;
    
    int result = execute(CONFIRM_DATE, fr.PasswordAdmin, p, a);
    return result;
}
//-----------------------------------------------------------------------------
/**
Инициализация таблиц начальными значениями
Команда:
24H. Длина сообщения: 5 байт.
Пароль системного администратора (4 байта)
Ответ:
24H. Длина сообщения: 2 байта.
Код ошибки (1 байт)
 * @return 
 */
int InitTable(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD InitTable");
    parameter p;
    answer a;

    int result = execute(INIT_TABLE, fr.PasswordAdmin, p, a);    
    return result;
}
//-----------------------------------------------------------------------------
/**
 Запись таблицы
Команда:
1EH. Длина сообщения: (9+X) байт.
Пароль системного администратора (4 байта)
Таблица (1 байт)
Ряд (2 байта)
Поле (1 байт)
Значение `,~,* (X байт) до 40 или до 246` байт
Ответ:
1EH. Длина сообщения: 2 байта.
Код ошибки (1 байт)
Примечание:
` – в зависимости от модели ККТ (для параметра модели Бит 23, см. команду F7H);
~ – для текстового значения: символы в кодовой странице WIN1251; символы с кодами 1...31
    игнорируются; символ '\0' (код 0) в строке принудительно обрезает строку;
* – для текстового значения: символ пробела ' ' (код 32) подвергается удалению 1 в
    соответствии с настройкой «УДАЛЯТЬ ВЕДУЩИЕ И КОНЕЧНЫЕ ПРОБЕЛЫ» в таблице 1.
 * @return 
 */
int WriteTable(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD WriteTable");
    parameter p;
    answer a;
    int len;
    char *tmp;

    int result = GetFieldStruct();
    printf("GetFieldStruct size:%i type:%i name:%s min:%i max:%i ---> %s[code %d]\n", fr.FieldSize, fr.FieldType, fr.FieldName, fr.MINValueOfField, fr.MAXValueOfField, fr.ResultCodeDescription, fr.ResultCode);

    if( result == 0 ){
        if (fr.FieldType == 1) {
            len = strlen(fr.ValueOfFieldString);
            tmp = fr.ValueOfFieldString;
        } else {
            len = fr.FieldSize;
            tmp = (char*) fr.ValueOfFieldInteger;
        };
        p.len = 9 + len;

        p.buff[0] = fr.TableNumber;
        memcpy(p.buff + 1, &fr.RowNumber, 2);
        p.buff[3] = fr.FieldNumber;
        memcpy(p.buff + 4, &tmp, len);
    
        result = execute(WRITE_TABLE, fr.PasswordAdmin, p, a);
    }
    return result;
}
//-----------------------------------------------------------------------------
/**
 Чтение таблицы
Команда: 1FH. Длина сообщения: 9 байт.
Пароль системного администратора (4 байта)
Таблица (1 байт)
Ряд (2 байта)
Поле (1 байт)
Ответ:
1FH. Длина сообщения: (2+X) байт.
Код ошибки (1 байт)
Значение (X байт) до 40 или до 246` байт
Примечание: ` – в зависимости от модели ККТ (для параметра модели Бит 23, см. команду F7H).
 * @return 
 */
int ReadTable(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ReadTable");
    parameter p;
    answer a;
    int len;

    p.len = 9;

    p.buff[0] = fr.TableNumber;
    memcpy(p.buff + 1, &fr.RowNumber, 2);
    p.buff[3] = fr.FieldNumber;

    int result = execute(READ_TABLE, fr.PasswordAdmin, p, a);
    if( result == 0 ){
        result = GetFieldStruct();
        printf("GetFieldStruct size:%i type:%i name:%s min:%i max:%i ---> %s[code %d]\n", fr.FieldSize, fr.FieldType, fr.FieldName, fr.MINValueOfField, fr.MAXValueOfField, fr.ResultCodeDescription, fr.ResultCode);
        if (fr.FieldType == 1) strncpy(fr.ValueOfFieldString, (char*) a.buff, len - 2);
        else fr.ValueOfFieldInteger = evalint64((unsigned char*) &a.buff + 2, fr.FieldSize);
    }
    return result;
}
//-----------------------------------------------------------------------------
/**
Запрос структуры поля
Команда:
2EH. Длина сообщения: 7 байт.
Пароль системного администратора (4 байта)
Номер таблицы (1 байт)
Номер поля (1 байт)
Ответ:
2EH. Длина сообщения: (44+X+X) байт.
Код ошибки (1 байт)
Название поля (40` байт)
Тип поля (1 байт) «0» – BIN, «1» – CHAR
Количество байт – X (1 байт)
Минимальное значение поля – для полей типа BIN (X байт)
Максимальное значение поля – для полей типа BIN (X байт)
Примечание: 
` – текст более короткого названия завершается символом '\0' (код 0).
 * @return 
 */
int GetFieldStruct(void) 
{
    
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetFieldStruct");
    parameter p;
    answer a;

    p.len = 7;

    p.buff[0] = fr.TableNumber;
    p.buff[1] = fr.FieldNumber;

    int result = execute(GET_FIELD_STRUCT, fr.PasswordAdmin, p, a);
    strncpy(fr.FieldName, (char*) &a.buff + 2, 40);

    fr.FieldType = a.buff[42];
    fr.FieldSize = a.buff[43];
    fr.MINValueOfField = evalint64((unsigned char*) &a.buff + 44, fr.FieldSize);
    fr.MAXValueOfField = evalint64((unsigned char*) &a.buff + 44 + fr.FieldSize, fr.FieldSize);

    return result;
}
//-----------------------------------------------------------------------------
/**
Запрос структуры таблицы
Команда:
2DH. Длина сообщения: 6 байт.
Пароль системного администратора (4 байта)
Номер таблицы (1 байт)
Ответ:
2DH. Длина сообщения: 45 байт.
Код ошибки (1 байт)
Название таблицы (40` байт)
Количество рядов (2 байта)
Количество полей (1 байт)
Примечание: 
` – текст более короткого названия завершается символом '\0' (код 0).
 * @return 
 */
int GetTableStruct(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetTableStruct");
    parameter p;
    answer a;

    p.len = 1;

    p.buff[0] = fr.TableNumber;

    int result = execute(GET_TABLE_STRUCT, fr.PasswordAdmin, p, a);

    strncpy(fr.TableName, (char*) &a.buff + 1, 40);
    fr.RowNumber = evalint((unsigned char*) &a.buff + 41, 2);
    fr.FieldNumber = a.buff[43];
    
    return result;
}
//-----------------------------------------------------------------------------

int WriteLicense(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD WriteLicense");
    parameter p;
    answer a;

    int64_t num = atoll(fr.License);
    p.len = 5;

    memcpy(p.buff, &num, 5);

    int result = execute(WRITE_LICENSE, fr.PasswordAdmin, p, a);
    return result;
}
//-----------------------------------------------------------------------------

int ReadLicense(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ReadLicense");
    parameter p;
    answer a;

    int result = execute(READ_LICENSE, fr.PasswordAdmin, p, a);
    
    sprintf(fr.License, "%.0lg", (double) evalint64((unsigned char*) &a.buff + 2, 5));

    return result;
}
//-----------------------------------------------------------------------------

int InitFM(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD InitFM");
    parameter p;
    answer a;
    
    int result = execute(INIT_FM, fr.PasswordAdmin, p, a);

    return result;
}
//-----------------------------------------------------------------------------

int Fiscalization(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Fiscalization");

    parameter p;
    answer a;

    int nti = fr.NewPasswordTI;
    int64_t rnm = atoll(fr.RNM);
    int64_t inn = atoll(fr.INN);
    p.len = 14;

    memcpy(p.buff, &nti, 4);
    memcpy(p.buff + 4, &rnm, 5);
    memcpy(p.buff + 9, &inn, 6);

    int result = execute(FISCALIZATION, fr.PasswordAdmin, p, a);

    fr.RegistrationNumber = evalint((unsigned char*) &a.buff + 2, 1);
    fr.FreeRegistration = evalint((unsigned char*) &a.buff + 3, 1);
    fr.SessionNumber = evalint((unsigned char*) &a.buff + 4, 2);
    evaldate((unsigned char*) &a.buff + 6, &fr.Date);

    return result;
}
//-----------------------------------------------------------------------------

int FiscalReportForDatesRange(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD FiscalReportForDatesRange");
    parameter p;
    answer a;

    p.len = 7;

    p.buff[0] = fr.ReportType;

    p.buff[1] = fr.FirstSessionDate.tm_year - 100;
    p.buff[2] = fr.FirstSessionDate.tm_mon + 1;
    p.buff[3] = fr.FirstSessionDate.tm_mday;

    p.buff[4] = fr.LastSessionDate.tm_year - 100;
    p.buff[5] = fr.LastSessionDate.tm_mon + 1;
    p.buff[6] = fr.LastSessionDate.tm_mday;

    int result = execute(FISCAL_REPORT_FOR_DATES_RANGE, fr.PasswordAdmin, p, a);

    evaldate((unsigned char*) &a.buff + 2, &fr.FirstSessionDate);
    evaldate((unsigned char*) &a.buff + 5, &fr.LastSessionDate);
    fr.FirstSessionNumber = evalint((unsigned char*) &a.buff + 8, 2);
    fr.LastSessionNumber = evalint((unsigned char*) &a.buff + 10, 2);

    return result;
}
//-----------------------------------------------------------------------------

int FiscalReportForSessionRange(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD FiscalReportForSessionRange");
    parameter p;
    answer a;


    p.len = 5;

    p.buff[0] = fr.ReportType;

    memcpy(p.buff + 1, &fr.FirstSessionNumber, 2);
    memcpy(p.buff + 3, &fr.LastSessionNumber, 2);
    
    int result = execute(FISCAL_REPORT_FOR_SESSION_RANGE, fr.PasswordAdmin, p, a);

    evaldate((unsigned char*) &a.buff + 2, &fr.FirstSessionDate);
    evaldate((unsigned char*) &a.buff + 5, &fr.LastSessionDate);
    fr.FirstSessionNumber = evalint((unsigned char*) &a.buff + 8, 2);
    fr.LastSessionNumber = evalint((unsigned char*) &a.buff + 10, 2);
    
    return result;
}
//-----------------------------------------------------------------------------

int InterruptFullReport(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD InterruptFullReport");

    parameter p;
    answer a;

    int result = execute(INTERRUPT_FULL_REPORT, fr.PasswordAdmin, p, a);

    return result;
}
//-----------------------------------------------------------------------------

int GetFiscalizationParameters(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetFiscalizationParameters");

    parameter p;
    answer a;

    p.len = 1;
    p.buff[0] = fr.RegistrationNumber;
    
    int result = execute(GET_FISCALIZATION_PARAMETERS, fr.PasswordAdmin, p, a);

    fr.NewPasswordTI = evalint((unsigned char*) &a.buff + 2, 4);
    sprintf(fr.RNM, "%.0lg", (double) evalint64((unsigned char*) &a.buff + 6, 5));
    sprintf(fr.INN, "%.0lg", (double) evalint64((unsigned char*) &a.buff + 11, 6));
    fr.SessionNumber = evalint((unsigned char*) &a.buff + 17, 2);
    evaldate((unsigned char*) &a.buff + 19, &fr.Date);

    return result;
}
//-----------------------------------------------------------------------------

int GetFMRecordsSum(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetFMRecordsSum");

    parameter p;
    answer a;

    p.len = 1;

    p.buff[0] = fr.TypeOfSumOfEntriesFM;

    int result = execute(GET_FM_RECORDS_SUM, fr.PasswordAdmin, p, a);

    fr.OperatorNumber = a.buff[2];
    fr.Summ1 = evalint64((unsigned char*) &a.buff + 3, 8);
    fr.Summ1 /= 100;
    fr.Summ2 = evalint64((unsigned char*) &a.buff + 11, 6);
    fr.Summ2 /= 100;
    fr.Summ3 = evalint64((unsigned char*) &a.buff + 17, 6);
    fr.Summ3 /= 100;
    fr.Summ4 = evalint64((unsigned char*) &a.buff + 23, 6);
    fr.Summ4 /= 100;

    return result;
}
//-----------------------------------------------------------------------------

int GetLastFMRecordDate(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetLastFMRecordDate");

    parameter p;
    answer a;

    int result = execute(GET_LAST_FM_RECORD_DATE, fr.PasswordAdmin, p, a);

    fr.OperatorNumber = a.buff[2];
    fr.LastFMRecordType = a.buff[3];
    evaldate((unsigned char*) &a.buff + 4, &fr.Date);

    return result;
}
//-----------------------------------------------------------------------------

int GetRangeDatesAndSessions(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetRangeDatesAndSessions");
    parameter p;
    answer a;

    int result = execute(GET_RANGE_DATES_AND_SESSIONS, fr.PasswordAdmin, p, a);

    evaldate((unsigned char*) &a.buff + 2, &fr.FirstSessionDate);
    evaldate((unsigned char*) &a.buff + 5, &fr.LastSessionDate);
    fr.FirstSessionNumber = evalint((unsigned char*) &a.buff + 8, 2);
    fr.LastSessionNumber = evalint((unsigned char*) &a.buff + 10, 2);

    return result;
}
//-----------------------------------------------------------------------------

int EKLZDepartmentReportInDatesRange(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int EKLZDepartmentReportInSessionsRange(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int EKLZJournalOnSessionNumber(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int EKLZSessionReportInDatesRange(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int EKLZSessionReportInSessionRange(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int ReadEKLZDocumentOnKPK(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int ReadEKLZSessionTotal(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int StopEKLZDocumentPrinting(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int Correction(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int DozeOilCheck(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int SummOilCheck(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int SetDozeInMilliliters(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int SetDozeInMoney(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int OilSale(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int GetLiterSumCounter(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int GetRKStatus(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int LaunchRK(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int StopRK(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int ResetRK(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int ResetAllTRK(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int SetRKParameters(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int EjectSlipDocument(void) 
{
    return 0;
}
//-----------------------------------------------------------------------------

int LoadLineData(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD LoadLineData");

    parameter p;
    answer a;

    p.len = 41;

    p.buff[0] = fr.LineNumber;
    memcpy(p.buff + 1, fr.LineData, 40);

    int result = execute(LOAD_LINE_DATA, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int LoadLineDataExt(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD LoadLineDataExt");
    parameter p;
    answer a;

    p.len = 82;

    memcpy(p.buff, &fr.LineNumber, 2);
    memcpy(p.buff + 2, fr.LineDataExt, 80);

    int result = execute(LOAD_LINE_DATA_EXT, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int Draw(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD Draw");
    parameter p;
    answer a;

    p.len = 2;

    p.buff[0] = fr.FirstLineNumber;
    p.buff[1] = fr.LastLineNumber;

    int result = execute(DRAW, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int DrawExt(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD DrawExt");
    parameter p;
    answer a;

    p.len = 5;

    memcpy(p.buff, &fr.FirstLineNumber, 2);
    memcpy(p.buff+2, &fr.LastLineNumber, 2);
/*
Флаги 1 (X 1 = 1 байт) Бит 0 – контрольная лента, Бит 1 – чековая лента, Бит 2 2 –
подкладной документ, Бит 3 3 – слип чек; Бит 7 4 – отложенная печать графики
 */
    p.buff[4] = 0;

    int result = execute(DRAW_EXT, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int PrintBarCode(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintBarCode");
    parameter p;
    answer a;

    int64_t barcode = atoll(fr.BarCode);
    p.len = 5;

    memcpy(p.buff, &barcode, 5);

    int result = execute(PRINT_BARCODE, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];

    return result;
}
//-----------------------------------------------------------------------------

int PrintLine(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintLine");
    parameter p;
    answer a;
    p.len = 65;

    memcpy(p.buff, &fr.RepiatLine, 2);
/*
Флаги 1 (1 байт) Бит 0 – контрольная лента, Бит 1 – чековая лента, Бит 2 2 –
подкладной документ, Бит 3 3 – слип чек; Бит 7 4 – отложенная печать графики
 */
    p.buff[2] = 0;

    memcpy(p.buff + 3, fr.LineData, 62);
    
    int result = execute(PRINT_LINE, fr.Password, p, a);
    return result;
}
//-----------------------------------------------------------------------------

/*
 * Печать многомерного штрих-кода
 * @return int
 */
int PrintBarCodeMultidimensional(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintBarCodeMultidimensional");
    printf(" ||-------> CMD PrintBarCodeMultidimensional\n");
    parameter p;
    answer a;
    /* Команда: DEH. Длина сообщения: 15 байт.
     * Пароль (4 байта)
     */
    p.len = 15;
    /* Тип штрих-кода (1 байт) 
     * 0 - PDF 417
     * 1 - DATAMATRIX
     * 2 - AZTEC
     * 3 - QR code
     * 131 - QR code
     */
    p.buff[0] = fr.BarCodeType;
    /* Длина данных штрих-кода (2 байта) 1...7089 1 */
    memcpy(p.buff+1, &fr.BarCodeDataLength, 2);
    /* Номер начального блока данных (1 байт) 0...127 */
    p.buff[3] = fr.BarCodeStartBlockNumber;
    /*
     * Номер параметра | PDF 417                 | DATAMATRIX      | AZTEC                  | QR Code
     */
    
    /* Параметр 1 (1 байт) 
     *        1        | Number of columns       | Encoding scheme | Encoding scheme        | Version,0=auto; 40 (max)
     */
    p.buff[4] = fr.BarcodeParameter1;
    
    
    /* Параметр 2 (1 байт) 
     *        2        | Number of rows          | Rotate          | -                      | Mask; 8 (max)
     */
    p.buff[5] = fr.BarcodeParameter2;
    
    
    /* Параметр 3 (1 байт) 
     *        3        | Width of module         | Dot size        | Dot size               | Dot size; 3...8
     */
    p.buff[6] = fr.BarcodeParameter3;
    
    
    /* Параметр 4 (1 байт) 
     *        4        | Module height           | Symbol size     | Symbol size            | -
     */
    p.buff[7] = fr.BarcodeParameter4;
    
    
    /* Параметр 5 (1 байт) 
     *        5        | Error correction level  | -               | Error correction level | Error correction level; 0...3=L,M,Q,H
     */
    p.buff[8] = fr.BarcodeParameter5;
    
    
    /* Выравнивание (1 байт) 
     * 0 - По левому краю
     * 1 - По центру
     * 2 - По правому краю
     */
    p.buff[9] = fr.BarCodeAlignment;
    
    
    /* Ответ: DEH. Длина сообщения: 3 байт или 12 2 байт.
     * Код ошибки (1 байт)
     */
    int result = execute(PRINT_MULTIDIMENSIONAL_BARCODE, fr.Password, p, a);
    /* Порядковый номер оператора (1 байт) 1...30 */
    fr.OperatorNumber = a.buff[2];
    /* Параметр 1 (1 байт) 2 */
    /* Параметр 2 (1 байт) 2 */
    /* Параметр 3 (1 байт) 2 */
    /* Параметр 4 (1 байт) 2 */
    /* Параметр 5 (1 байт) 2 */
    /* Размер штрих-кода (горизонтальный) в точках (2 байта) 2 */
    /* Размер штрих-кода (вертикальный) в точках (2 байта) */
    return result;
}
//-----------------------------------------------------------------------------

std::string PrintBarCode128ByLine(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintBarCode128ByLine" );

    string bitStr;
    barcode bc(fr.BarCode, A);
    bc.format();
    bitStr = bc.formattedData;

    //cin.ignore();

    string reverseBinStr;

    std::stringstream ss;
    std::string scaleStr;

    for (std::string::iterator it = bitStr.begin(); it != bitStr.end(); ++it) {
        // for scale
        if (*it == '0') {
            scaleStr = scaleStr + "000";

        } else {
            scaleStr = scaleStr + "111";
        }
    }
    std::cout << "^^^ barcode line" << scaleStr <<" ^^^^" << std::endl;

    std::string BarCode(fr.BarCode);
    std::string appendBarCode;

    for (int s = 0; s < BarCode.length(); s++) {
        if (s != 0) {
            appendBarCode.insert(appendBarCode.length(), 1, ' ') += BarCode[s];
        } else {
            appendBarCode += BarCode[s];
        }
    }

    appendBarCode.insert(0, ((40 - appendBarCode.length()) / 2) + 2, ' '); // with margin
    fr.UseReceiptRibbon = true;
    fr.StringForPrinting = (char *) appendBarCode.c_str();

    int result = PrintString();
    if( result != 0  ){
        std::cout << "^^^ ERROR PRINT BAR CODE STRING ^^^^" << std::endl;
    } else {
        std::cout << "^^^ SUCCESS PRINT BAR CODE STRING ^^^^" << std::endl;
    }

    for (unsigned i = 0; i < scaleStr.length(); i += 8) {
        reverseBinStr = scaleStr.substr(i, 8);
        reverse(reverseBinStr.begin(), reverseBinStr.end());
        string binary_str(reverseBinStr);
        bitset<8> set(binary_str);
        ss << std::uppercase << std::hex << set.to_ulong() << endl;
    }

    std::istringstream hex_chars_stream(ss.str());
    std::vector<unsigned char> bytes;
    int line = 0;
    unsigned int c;
    memset(fr.LineData, 0, 62);
    while (hex_chars_stream >> std::hex >> c) {
        bytes.push_back(c);
        memcpy(fr.LineData + line + (62 - (scaleStr.length() / 8)) / 2, &c, 1); // /with margin

        line++;
    }

    fr.RepiatLine = 120;
    PrintLine();

    return fr.BarCode;
}
//-----------------------------------------------------------------------------

std::string PrintQRCode(void)
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintQRCode");
    //LOG4CXX_DEBUG( __glb_logger, " QRCode: " << fr.BarCode);
    printf(" ||-------> CMD PrintQRCode\n");
    // сгенерировать данные для бар кода из fr.BarCode;
    fr.DownloadDataType = 0;//Тип данных (1 байт) 0 – данные для двумерного штрих-кода
    memcpy(fr.DownloadData,fr.BarCode,64);
    std::cout << fr.DownloadData << std::endl;
    fr.DownloadDataBlockNumber = 0;
    // Загрузить часть данных на принтер
    fn.DownloadData();
    
    // Устанавливает значения для печати qr кода
    fr.BarCodeType = 3;
    fr.BarCodeDataLength = strlen(fr.BarCode);
    fr.BarCodeStartBlockNumber = 0;
    
    // Version,0=auto; 40 (max)
    if( fr.BarcodeParameter1 < 0 || fr.BarcodeParameter1 > 40 ){
        fr.BarcodeParameter1 = 0;
    }
    // Mask; 8 (max)
    if( fr.BarcodeParameter2 < 0 || fr.BarcodeParameter2 > 8 ){
        fr.BarcodeParameter2 = 8;
    }
    
    // Dot size; 3...8
    if( fr.BarcodeParameter3 < 0 || fr.BarcodeParameter3 > 8 ){
        fr.BarcodeParameter3 = 8;
    }
    fr.BarcodeParameter4 = 0;
    
    //Error correction level; 0...3=L,M,Q,H
    if( fr.BarcodeParameter5 < 0 || fr.BarcodeParameter5 > 3 ){
        fr.BarcodeParameter5 = 3;
    }
    
    // Выравнивание 0..2
    if( fr.BarCodeAlignment < 0 || fr.BarCodeAlignment > 2 ){
        fr.BarCodeAlignment = 1;
    }
    
    fn.PrintBarCodeMultidimensional();
    
    return fr.BarCode;
}
//-----------------------------------------------------------------------------

int PrintBmpImageExt(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintBmpImage");

    int i = 0, j = 0;
    //BITMAPFILEHEADER fileHeader;
    //BITMAPINFOHEADER fileInfoHeader;
    //LOG4CXX_DEBUG( __glb_logger," PrintBmpImage: try read_bmp" );
    //LOG4CXX_DEBUG( __glb_logger," -- image " << fr.BmpImage );
    //RGBQUAD **rgbInfo = read_bmp(fr.BmpImage, fileHeader, fileInfoHeader);
    //std::cout << " -- bits " << fileInfoHeader.biBitCount << std::endl;
    //std::cout << " -- size " << fileInfoHeader.biWidth << " x " << fileInfoHeader.biHeight << std::endl;


    int k, rev_j;

    int w = 250;
    int h = 1;


    string reverseBinStr;
    string bitStr;

    for (j = 0; j < h; j++) {
        bitStr = "";
        for (i = 0; i < w; i++) {
            bitStr += (i % 2 == 0 && j % 2 == 0) ? "000" : "111";
        }

        std::stringstream ss;
        for (unsigned i = 0; i < bitStr.length(); i += 8) {
            reverseBinStr = bitStr.substr(i, 8);
            reverse(reverseBinStr.begin(), reverseBinStr.end());
            string binary_str(reverseBinStr);
            bitset<8> set(binary_str);
            ss << std::uppercase << std::hex << set.to_ulong() << endl;
        }

        memset(fr.LineDataExt, 0, 250);

        std::istringstream hex_chars_stream(ss.str());
        std::vector<unsigned char> bytes;
        int line = 0;
        unsigned int c;
        while (hex_chars_stream >> std::hex >> c) {
            bytes.push_back(c);

            memcpy(fr.LineDataExt + line, &c, 1);
            line++;
        }
        fr.LineNumber = j;
        LoadLineDataExt();
    }

    fr.FirstLineNumber = 1;
    fr.LastLineNumber = h;
    
    return DrawExt();
}
//-----------------------------------------------------------------------------

int PrintBmpImage(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD PrintBmpImage");

    unsigned int i = 0, j = 0, lN = 0, squard = 3;
    //BITMAPFILEHEADER fileHeader;
    //BITMAPINFOHEADER fileInfoHeader;
    //LOG4CXX_DEBUG( __glb_logger, " PrintBmpImage: try read_bmp" );
    //LOG4CXX_DEBUG( __glb_logger, " -- image " << fr.BmpImage << " with zoom " << fr.BmpImageSquard );
    if( fr.BmpImageSquard <= 0 || fr.BmpImageSquard >= 5 ) {
        fr.BmpImageSquard = 1;
    }
    //RGBQUAD **rgbInfo = read_bmp(fr.BmpImage, fileHeader, fileInfoHeader);
    //std::cout << " -- bits " << fileInfoHeader.biBitCount << std::endl;
    //std::cout << " -- size " << fileInfoHeader.biWidth << " x " << fileInfoHeader.biHeight << std::endl;

    std::string file_name(fr.BmpImage);

    bitmap_image image(file_name);

    if (!image)
    {
       printf("Error - Failed to open '%s'\n",file_name.c_str());
       //LOG4CXX_ERROR( __glb_logger,"Error - Failed to open " << file_name.c_str());
       return -404;
    }

    int k, rev_j;

    unsigned int w = image.width();
    unsigned int h = image.height();


    string reverseBinStr;
    string bitStr;
    std::stringstream ss;

    for (j = 0; j < h; j++) {
        
        ss.str("");
        bitStr = "";
        
        for (i = 0; i < w; i++) {
            rgb_t colour;
            image.get_pixel(j, i, colour);

            char d;
            if( (int)colour.red == 255 && (int)colour.green == 255 && (int)colour.blue == 255 ){
                d = '0';
            } else {
                d = '1';
            }
            char *c = new char[fr.BmpImageSquard+1];
            memset (c, d, fr.BmpImageSquard);
            c[fr.BmpImageSquard] = '\0';
            bitStr += c;
            delete(c);
        }
        
        //std::cout << endl;
        //LOG4CXX_DEBUG( __glb_logger, "[" << bitStr << "]" );

        ss.str("");
        for ( i = 0; i < bitStr.length(); i += 8) {
            reverseBinStr = bitStr.substr(i, 8);
            reverse(reverseBinStr.begin(), reverseBinStr.end());
            string binary_str(reverseBinStr);
            bitset<8> set(binary_str);
            ss << std::uppercase << std::hex << set.to_ulong() << endl;
        }
        
        i = 0;
        while(i<fr.BmpImageSquard){
            
            memset(fr.LineData, 0, 80);

            std::istringstream hex_chars_stream(ss.str());
            std::vector<unsigned char> bytes;

            int line = 0;
            unsigned int c;
            while (hex_chars_stream >> std::hex >> c) {
                bytes.push_back(c);
                memcpy(fr.LineData + line, &c, 1);
                line++;
            }
            fr.LineNumber = lN;
            lN++;
            LoadLineData();
            //std::cout << "-------attemp " << i << "LineData[" << std::hex << fr.LineData << "]" << endl;
            //std::cout << "-------attemp " << i << "line[" << std::dec << line << "]" << endl;
        
            i++;
        }
    }

    fr.FirstLineNumber = 1;
    fr.LastLineNumber = lN;
    //LOG4CXX_DEBUG( __glb_logger,  "-------start[" << 1 << "]" );
    //LOG4CXX_DEBUG( __glb_logger,  "-------stop[" << lN << "]" );


    return Draw();
    //return 0;
}
//-----------------------------------------------------------------------------

int OpenShift(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD OpenShift");

    int len = 0;
    parameter p;
    answer a;
    p.len = 1;
    
    int result = execute(OPEN_SHIFT, fr.Password, p, a);
    fr.OperatorNumber = a.buff[2];
    
    return result;
}
//-----------------------------------------------------------------------------

/**
 * FF01h
 * Запрос статуса ФН
 * @return int
 */
int GetFNStatus(void)
{
    
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetFNStatus");

    parameter p;
    answer a;
    p.len = 6;
    a.cmd_len = 2;
    p.cmd_len = 2;
    /*
     * Код команды FF01h. Длина сообщения: 6 байт.
     *  Пароль системного администратора: 4 байта
     */

    int result = execute(GET_FN_STATUS_L, fr.PasswordAdmin, p, a);
/*
    Ответ: FF01h Длина сообщения: 31 байт.
    Код ошибки: 1 байт
*/

    /*
     * Состояние фазы жизни: 1 байт
     *  Бит 0 – проведена настройка ФН
     *  Бит 1 – открыт фискальный режим
     *  Бит 2 – закрыт фискальный режим
     *  Бит 3 – закончена передача фискальных данных в ОФД
     */
    fr.FNLifeState = evalint((unsigned char*) &a.buff + 3, 1);
    
    /*
     * Текущий документ: 1 байт
     *  00h – нет открытого документа
     *  01h – отчет о фискализации
     *  02h – отчет об открытии смены
     *  04h – кассовый чек
     *  08h – отчет о закрытии смены
     *  10h – отчет о закрытии фискального режима
     *  11h – Бланк строкой отчетности
     *  12h - Отчет об изменении параметров регистрации ККТ в связи с заменой ФН
     *  13h – Отчет об изменении параметров регистрации ККТ
     *  14h – Кассовый чек коррекции
     *  15h – БСО коррекции
     *  17h – Отчет о текущем состоянии расчетов
     */
    fr.FNCurrentDocument = evalint((unsigned char*) &a.buff + 4, 1);
    /*
     * Данные документа: 1 байт
     *  00 – нет данных документа
     *  01 – получены данные документа
     */
    fr.FNDocumentData = evalint((unsigned char*) &a.buff + 5, 1);

    /*
     * Состояние смены: 1 байт
     *  00 – смена закрыта
     *  01 – смена открыта
     */
    fr.ShiftStatus = evalint((unsigned char*) &a.buff + 6, 1);
    fr.FNSessionState = evalint((unsigned char*) &a.buff + 6, 1);
    
    // Флаги предупреждения: 1 байт
    fr.FNWarningFlags = evalint((unsigned char*) &a.buff + 7, 1);
    // Дата и время: 5 байт
    /*
    evaldate((unsigned char*) &a.buff + 8, &fr.Date);
    evaltime((unsigned char*) &a.buff + 8, &fr.Time);
     */
    // Номер ФН: 16 байт ASCII
    //sprintf(fr.SerialNumber, "%d", evalint((unsigned char*) &a.buff + 13, 16));
    // Номер последнего ФД: 4 байта
    fr.DocumentNumber = evalint((unsigned char*) &a.buff + 29, 4);
    return result;
}
//-----------------------------------------------------------------------------

int CancelPrint(void) 
{
    return 0;
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD CancelPrint");

    parameter p;
    answer a;
    
    p.buff[0] = 0x11;
    p.buff[1] = 0x01;
    p.buff[2] = 0x00;
    p.buff[3] = 0x07;
    p.buff[4] = 0x01;
           
    int result = execute(WRITE_TABLE, fr.Password, p, a);
    
    return result;
}
//-----------------------------------------------------------------------------

/*
 * Сброс состояния ФН
 */
int ResetFnState(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetInformExchangeStatus" );

    int len = 0;
    parameter p;
    answer a;
    p.len = 7;
    p.cmd_len = 2;

    a.cmd_len = 2;
    /* Код команды FF07h . Длина сообщения: 7 байт.
     * Пароль системного администратора: 4 байта
     */

    /* Код запроса: 1 байт */
    p.buff[0] = 0;//20;
    int result = execute(RESET_FN_STATE_L, fr.PasswordAdmin, p, a);
    /* Ответ: FF07h Длина сообщения: 1 байт.
     * Код ошибки: 1 байт
     */

    return result;
}
//-----------------------------------------------------------------------------

/*
 * Получить статус информационного обмена
 */
int GetInformExchangeStatus(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetInformExchangeStatus" );

    int len = 0;
    parameter p;
    answer a;
    p.len = 6;
    p.cmd_len = 2;

    a.cmd_len = 2;
    
    /* Код команды FF39h . Длина сообщения: 6 байт.
     * Пароль системного администратора: 4 байта
     */
    int result = execute(GET_STATE_INFORM_EXCHANGE_L, fr.PasswordAdmin, p, a);
    /* Ответ: FF39h Длина сообщения: 14 байт.
     * Код ошибки: 1 байт
     */
    
    /*
     * Статус информационного обмена: 1 байт (0 – нет, 1 – да)
     *  Бит 0 – транспортное соединение установлено
     *  Бит 1 – есть сообщение для передачи в ОФД
     *  Бит 2 – ожидание ответного сообщения (квитанции) от ОФД
     *  Бит 3 – есть команда от ОФД
     *  Бит 4 – изменились настройки соединения с ОФД
     *  Бит 5 – ожидание ответа на к оманду от ОФД
     */
    
    /* Состояние чтения сообщения: 1 байт 1 – да, 0 -нет */

    /* Количество сообщений для ОФД: 2 байта */

    /* Номер документа для ОФД первого в очереди: 4 байта */

    /* Дата и время документа для ОФД первого в очереди: 5 байт */
    
    return result;
}
//-----------------------------------------------------------------------------

/**
 * Загрузка данных
 */
int DownloadData(void) 
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD DownloadData");
     printf(" ||-------> CMD DownloadData\n");
    int len = 0;
    parameter p;
    answer a;
    p.len = 71;
    
    /*
     * Команда: DDH. Длина сообщения: 71 байт.
     * Пароль (4 байта)
     */

    /* Тип данных (1 байт) 0 – данные для двумерного штрих-кода */
    p.buff[0] = fr.DownloadDataType;

    /* Порядковый номер блока данных (1 байт) 0...127 */
    p.buff[1] = fr.DownloadDataBlockNumber;
    
    /* Данные (64 байта) */
    memcpy(p.buff + 2, fr.DownloadData, 64);

    int result = execute(DOWNLOAD_DATA, fr.Password, p, a);
    /* Ответ: DDH. Длина сообщения: 3 байта.
     *  Код ошибки (1 байт)
     */

    /* Порядковый номер оператора (1 байт) 1...30 */
    fr.OperatorNumber = a.buff[2];
    
    return result;
}
//-----------------------------------------------------------------------------

/**
* В Таблице 24 «Встраиваемая и интернет техника», установите:
* • поле 3 «Выброс чеков» - значение 0 (наружу),
* • поле 4 «Выброс отчетов» - значение 1 (в ретрактор) или 0 (наружу),
* • поле 7 «Делать петлю при печати» - значение 1 (делать).
 * 
 * // Номер таблицы,Ряд,Поле,Размер поля,Тип поля,Мин. значение, Макс.значение, Название,Значение
 * 24,1,3,1,0,0,2,'Выброс чеков','0'
 * 24,1,4,1,0,0,1,'Выброс отчетов','0'
 * 
 * !!!!!!!!!!!!!!!!!!!!!!!! необходимо что бы у принтера в настройках был выставлен верный Paper Retracting 
 * так же в таблице 1 фискальника есть поле отрезка чека должно быть 1
*/
/**
 * 
 * @return 
 */
int SetPrintReceiptToOut(void)
{
    fr.TableNumber = 24;
    fr.RowNumber = 1;
    fr.FieldNumber = 3;
    fr.FieldSize = 1;
    fr.FieldType = 0;
        
    int result = WriteTable();
    printf("WriteTable #%i field #%i row #%i = [%i] ---> %s[code %d]\n", fr.TableNumber, fr.FieldNumber, fr.RowNumber, fr.ValueOfFieldInteger, fr.ResultCodeDescription, fr.ResultCode);

    return result;
}
//-----------------------------------------------------------------------------
/**
 * 
 * @return 
 */
int SetPrintReportToOut(void)
{
    fr.TableNumber = 24;
    fr.RowNumber = 1;
    fr.FieldNumber = 4;
    fr.FieldSize = 1;
    fr.FieldType = 0;
    
    int result = WriteTable();
    printf("WriteTable #%i field #%i row #%i = [%i] ---> %s[code %d]\n", fr.TableNumber, fr.FieldNumber, fr.RowNumber, fr.ValueOfFieldInteger, fr.ResultCodeDescription, fr.ResultCode);

    return result;
}
//-----------------------------------------------------------------------------
/**
 * 
 * @return 
 */
int GetPrintReceiptToOut(void)
{
    fr.TableNumber = 24;
    fr.RowNumber = 1;
    fr.FieldNumber = 3;
    fr.FieldSize = 1;
    fr.FieldType = 0;
        
    int result = ReadTable();
    printf("ReadTable #%i field #%i row #%i = [%i] ---> %s[code %d]\n", fr.TableNumber, fr.FieldNumber, fr.RowNumber, fr.ValueOfFieldInteger, fr.ResultCodeDescription, fr.ResultCode);

    return result;
}
//-----------------------------------------------------------------------------
/**
 * 
 * @return 
 */
int GetPrintReportToOut(void)
{
    fr.TableNumber = 24;
    fr.RowNumber = 1;
    fr.FieldNumber = 4;
    fr.FieldSize = 1;
    fr.FieldType = 0;
    
    int result = ReadTable();
    printf("ReadTable #%i field #%i row #%i = [%i] ---> %s[code %d]\n", fr.TableNumber, fr.FieldNumber, fr.RowNumber, fr.ValueOfFieldInteger, fr.ResultCodeDescription, fr.ResultCode);

    return result;
}
//-----------------------------------------------------------------------------

/**
 * 
 Запросить о наличие данных в буфере
Код команды FF30h . Длина сообщения: 6 байт.
Пароль системного администратора: 4 байта
Ответ:
FF30h Длина сообщения: 4 байта.
Код ошибки:1 байт
Количество байт в буфере: 2 байта 0 – нет данных
Максимальный размер блока данных: 1 байт
 * @return 
 */
int GetBufferSize(void)
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD GetBufferSize" );
    parameter p;
    answer a;
    p.len = 6;
    p.cmd_len = 2;

    a.cmd_len = 2;
    /* Код команды FF30h . Длина сообщения: 6 байт.
     * Пароль системного администратора: 4 байта
     */

    int result = execute(GET_BUFFER_SIZE_L, fr.PasswordAdmin, p, a);
    /* Ответ: FF30h Длина сообщения: 4 байт.
     * Код ошибки:1 байт
     * Количество байт в буфере: 2 байта 0 – нет данных
     * Максимальный размер блока данных: 1 байт
     */
    fr.BufferSize = evalint((unsigned char*) &a.buff+2, 4);;

    fr.BufferBlockMaxSize = a.buff[3];

    return result;
}

/**
 * 
Прочитать блок данных из буфера
Код команды FF31h . Длина сообщения: 9 байт.
Пароль системного администратора: 4 байта
Начальное смещение: 2 байта
Количество запрашиваемых данных :1 байт
Ответ:
FF31h Длина сообщения: 1+N байт.
Код ошибки: 1 байт
 * @return 
 */
int ReadBlockFromBuffer(void)
{
    //LOG4CXX_DEBUG( __glb_logger, " ||-------> CMD ReadBlockFromBuffer" );
    int result = 0;
    // READ_BLOCK_FROM_BUFFER_L
    return result;
}

int PrintFromBuffer(void)
{
    int result = 0;
    return result;
}