/***************************************************************************
                          conn.h  -  description
                             -------------------
    begin                : Thu Jan 10 2002
    copyright            : (C) 2002 by Igor V. Youdytsky
    email                : Pitcher@gw.tander.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _CONN_H
#define _CONN_H
#include <map>
#include <termios.h>
#include "drvfr.h"

#define MICRO_TO_MILLI         1000 
#define MAX_CONNECT_TRIES      15 // кол-во попыток при отправке ENQ
#define MAX_EXECUTE_TRIES      3  // кол-во повторных поппыток выполнить комманду в случае ошибок соединения(устройство ответило)
#define MAX_RESEND_TRIES       10 // кол-во повторных поппыток отправить команду
#define MAX_TRIES              10 // кол-во попыток отправить/получить данные от устройства
#define USE_AUTO_TIMEOUT       1 // Использовать увеличение таймаута
#define STEP_AUTO_TIMEOUT      5 // Шаг увеличения таймаута 
#define MAX_AUTO_TIMEOUT       80 // Величина до которой можно увеличить автоматически(при неудачи подключения к устройству или не выполнении команды) таймаут для отправки 1 байта.

#define ENQ    0x05
#define STX    0x02
#define ACK    0x06
#define NAK    0x15

using namespace std;

extern int __glb_connected;

/**********************************************************
 *  The length of commands                                *
 **********************************************************/
const unsigned commlen[0x100] ={
    //0    1    2    3    4    5    6    7    8    9    a    b    c    d    e    f
    0,     6,   5,   5,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x00 - 0x0f
    5,     5,   26,  5,   8,   6,   1,   46,  37,  6,   6,   6,   10,  5,   255, 9,  // 0x10 - 0x1f
    6,     8,   8,   8,   5,   6,   6,   5,   6,   7,   5,   5,   5,   6,   7,   47, // 0x20 - 0x2f
    0,     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x30 - 0x3f
    5,     5,   3,   5,   5,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x40 - 0x4f
    10,    10,  5,   6,   5,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x50 - 0x5f
    9,     1,   6,   5,   5,   20,  12,  10,  5,   6,   0,   0,   0,   0,   0,   0,  // 0x60 - 0x6f
    255,   255, 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x70 - 0x7f
    60,    60,  60,  60,  60,  71,  54,  54,  5,   5,   54,  54,  5,   6,   131, 0,  // 0x80 - 0x8f
    61,    57,  52,  11,  12,  52,  7,   7,   7,   5,   13,  7,   0,   0,   7,   7,  // 0x90 - 0x9f
    13,    11,  12,  10,  10,  8,   7,   5,   0,   0,   0,   0,   0,   0,   0,   0,  // 0xa0 - 0xaf
    5,     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0xb0 - 0xbf
    46,    7,   10,  9,   255, 47,  5,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0xc0 - 0xcf
    0,     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   71,  15,  0,  // 0xd0 - 0xdf
    5,     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0xe0 - 0xef
    15,    6,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   255, 0,   0,  // 0xf0 - 0xff
};

/**********************************************************
 *  timeout for prepare data on device                    *
 **********************************************************/
const unsigned wait_device[0x100] ={
    //0    1      2      3      4      5      6      7      8      9      a      b      c      d      e      f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x00 - 0x0f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x10 - 0x1f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x20 - 0x2f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x30 - 0x3f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x40 - 0x4f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x50 - 0x5f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x60 - 0x6f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x70 - 0x7f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x80 - 0x8f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0x90 - 0x9f
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0xa0 - 0xaf
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0xb0 - 0xbf
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0xc0 - 0xcf
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0xd0 - 0xdf
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, // 0xe0 - 0xef
    30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000  // 0xf0 - 0xff
};

struct CommandDescription {
    const char* title; //
    const char* code;
    int execute_timeout; // таймаут выполнения команды msec
    int len_code; // длина самой команды
    int len_min; // длина всей команды
    int len_max; // 
    int len_answer_min; // длина всего ответа
    int len_answer_max; //
    
    CommandDescription(const char* _code, int _len_code, int _execute_timeout, int _len_min, int _len_max, int _len_answer_min, int _len_answer_max, const char* _title) :
    code(_code), len_code(_len_code), execute_timeout(_execute_timeout), title(_title), len_min(_len_min), len_max(_len_max) {}
};

typedef std::map<const char*, CommandDescription > CommandMap;
extern CommandMap _commands;
void create_map_commands();

enum {
    s2400 = 0, s4800, s9600, s19200, s38400, s57600, s115200
};

enum {
    WAIT_READ = 0x01, WAIT_WRITE
};

const speed_t LineSpeedVal[10] ={B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B921600};

struct command {
    int len;
    unsigned char buff[260];
    int cmd_len;
    command(){
        len=0;
        cmd_len=1;
    }
} ;

struct answer {
    int len;
    unsigned char buff[260];
    int cmd_len;
    answer(){
        len=0;
        cmd_len=1;
    }
} ;

struct parameter{
    int len;
    unsigned char buff[260];
    int pass;
    int cmd_len;
    parameter(){
        len=0;
        cmd_len=1;
    }
};

void auto_increase_timeout();
int opendev(fr_prop*);
int closedev(void);
int readanswer(int, answer*);
int sendcommand(unsigned int, parameter*);
int checkstate(void);
int clearanswer(void);
void compose_cmd(unsigned int, unsigned int, unsigned char*);
#endif