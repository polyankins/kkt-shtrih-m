/***************************************************************************
                          errdefs.h  -  description
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002 by Igor V. Youdytsky
    email                : Pitcher@gw.tander.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef ERRDEFS_H
#define ERRDEFS_H

#include <string.h>

extern const char *errmsg[201];
/*******************/
/* Коды ошибок ФН  */
/*******************/
extern const char *errmsgfn[21];



/******* END ******/

extern const char *ecrmodedesc[16];
//
extern const char *ecrmode8desc[4];

extern const char *ecrsubmodedesc[6];

extern const char *devcodedesc[7];

/*
 */
extern const char *currdocdesc[24];

const char * get_message(const int num, const char *m_arr[], const char m_count);

const char * get_currdocdesc_message(const int num);

const char * get_submodedesc_message(const int num);

#endif