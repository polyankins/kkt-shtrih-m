/***************************************************************************
                          drvfr.h  -  description
                             -------------------
    begin                : Sun Jan 20 2002
    copyright            : (C) 2002 by Igor V. Youdytsky
    email                : Pitcher@gw.tander.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DRVFR_H
#define _DRVFR_H

#include <time.h>
#include <vector>
#include <string>
using namespace std;

/****************************************************
 * FR connamds                                      *
 ****************************************************/
//                                      Код     Название команды
#define _UNKNOWN_			0x00    // для команд которые не совпадают с отправленными
#define DUMP_REQUEST			0x01    // Запрос дампа
#define GET_DATA			0x02    // Запрос данных
#define INTERRUPT_DATA_STREAM		0x03    // Прерывание выдачи данных
#define GET_SHORT_STATUS		0x10    // Короткий запрос состояния
#define GET_ECR_STATUS			0x11    // Запрос состояния ККТ
#define PRINT_WIDE_STRING		0x12    // Печать жирной строки (шрифт 2)
#define BEEP				0x13    // Гудок
#define SET_EXCHANGE_PARAM		0x14    // Установка параметров обмена
#define GET_EXCHANGE_PARAM		0x15    // Чтение параметров обмена
#define RESET_SETTINGS			0x16    // Технологическое обнуление
#define PRINT_STRING			0x17    // Печать стандартной строки (шрифт 1)
#define PRINT_DOCUMENT_TITLE		0x18    // Печать заголовка документа
#define TEST				0x19    // Тестовый прогон
#define GET_CASH_REG			0x1a    // Запрос денежного регистра
#define GET_OPERATION_REG		0x1b    // Запрос операционного регистра
#define WRITE_LICENSE			0x1c    // 
#define READ_LICENSE			0x1d    //
#define WRITE_TABLE			0x1e    // Запись таблицы
#define READ_TABLE			0x1f    // Чтение таблицы
#define SET_POINT_POSITION		0x20    //
#define SET_TIME			0x21    // Программирование времени
#define SET_DATE			0x22    // Программирование даты
#define CONFIRM_DATE			0x23    // Подтверждение программирования даты
#define INIT_TABLE			0x24    // Инициализация таблиц начальными значениями
#define CUT_CHECK			0x25    // Отрезка чека
#define READ_FONT_PARAMS		0x26    // Прочитать параметры шрифта
#define RESET_SUMMARY			0x27    // Общее гашение
#define OPEN_DRAWER			0x28    // Открыть денежный ящик
#define FEED_DOCUMENT			0x29    // Протяжка
#define EJECT_SLIP_DOC			0x2a    //
#define INTERRUPT_TEST			0x2b    // Прерывание тестового прогона
#define PRINT_OPERATION_REG		0x2c    // Снятие показаний операционных регистров
#define GET_TABLE_STRUCT		0x2d    // Запрос структуры таблицы
#define GET_FIELD_STRUCT		0x2e    // Запрос структуры поля
#define PRINT_WITH_FONT_NUMBER		0x2ef   // Печать строки данным шрифтом
#define PRINT_REPORT_WITHOUT_CLEANING	0x40    // Суточный отчет без гашения
#define PRINT_REPORT_WITH_CLEANING	0x41    // Суточный отчет с гашением
#define PRINT_REPORT_BY_SECTIONS	0x42    // Отчёт по секциям
#define PRINT_REPORT_BY_TAX     	0x43    // Отчёт по налогам
#define PRINT_REPORT_BY_CASHIER 	0x44    // Отчёт по кассирам
#define CASH_INCOME			0x50    // Внесение
#define CASH_OUTCOME			0x51    // Выплата
#define PRINT_CLICHE			0x52    // Печать клише
#define END_DOCUMENT			0x52    // Конец Документа
#define PRINT_ADV_TEXT			0x52    // Печать рекламного текста
#define SET_SERIAL_NUMBER		0x60    // Ввод заводского номера
#define INIT_FM				0x61
#define GET_FM_RECORDS_SUM		0x62
#define GET_LAST_FM_RECORD_DATE		0x63
#define GET_RANGE_DATES_AND_SESSIONS	0x64
#define FISCALIZATION			0x65
#define FISCAL_REPORT_FOR_DATES_RANGE	0x66
#define FISCAL_REPORT_FOR_SESSION_RANGE	0x67
#define INTERRUPT_FULL_REPORT		0x68
#define GET_FISCALIZATION_PARAMETERS	0x69
#define FISCAL_PRINT_SLIP_DOC		0x70
#define PRINT_SLIP_DOC			0x71
#define SALE				0x80    // Продажа
#define BUY				0x81    // Покупка
#define RETURN_SALE			0x82    // Возврат продажи
#define RETURN_BUY			0x83    // Возврат покупки
#define STORNO				0x84    // Сторно
#define CLOSE_CHECK			0x85    // Закрытие чека
#define DISCOUNT			0x86    // Скидка
#define CHARGE				0x87    // Надбавка
#define CANCEL_CHECK			0x88    // Сторно надбавки
#define CHECK_SUBTOTAL			0x89    // Подытог чека
#define STORNO_DISCOUNT			0x8a    // Сторно скидки
#define STORNO_CHARGE			0x8b    // Сторно надбавки
#define PRINT_RECEIPT_COPY              0x8c    // Печать копии чека (Повтор документа)
#define OPEN_CHECK                      0x8d    // Открыть чек
#define CLOSE_CHECK_EXT                 0x8e    // Закрытие чека расширенное
#define DOZE_OIL_CHECK			0x90
#define SUMM_OIL_CHECK			0x91
#define CORRECTION			0x92
#define SET_DOZE_IN_MILLILITERS		0x93
#define SET_DOZE_IN_MONEY		0x94
#define OIL_SALE			0x95
#define STOP_RK				0x96
#define LAUNCH_RK			0x97
#define RESET_RK			0x98
#define RESET_ALL_TRK			0x99
#define SET_RK_PARAMETERS		0x9a
#define GET_LITER_SUM_COUNTER		0x9b
#define GET_CURRENT_DOZE		0x9e
#define GET_RK_STATUS			0x9f
#define ECT_DEP_DATE_REP		0xa0
#define ECT_DEP_SHIFT_REP		0xa1
#define ECT_SHIFT_DATE_REP		0xa2
#define ECT_SHIFT_SHIFT_REP		0xa3
#define ECT_SHIFT_TOTAL			0xa4
#define ECT_PAY_DOC_BY_NUMBER		0xa5
#define ECT_BY_SHIFT_NUMBER		0xa6
#define ECT_REPORT_INTR			0xa7
#define CONTINUE_PRINTING		0xb0    // Продолжение печати
#define LOAD_LINE_DATA			0xc0    // Загрузка графики
#define DRAW				0xc1    // Печать графики
#define PRINT_BARCODE			0xc2    // Печать штрих-кода EAN-13
#define DRAW_EXT                        0xc3    // Печать расширенной графики
#define LOAD_LINE_DATA_EXT              0xc4    // Загрузка расширенной графики
#define PRINT_LINE                      0xc5    // Печать графической линии
#define PRINT_REPORT_WITH_CLEANING_TO_BUFFER  0xc6 //Суточный отчет с гашением в буфер
#define DOWNLOAD_DATA                   0xdd    // Загрузка данных
#define PRINT_MULTIDIMENSIONAL_BARCODE  0xde    // Печать многомерного штрих-кода
#define OPEN_SHIFT			0xe0    // Открыть смену
#define GET_DEVICE_METRICS		0xfc    // Получить тип устройства
#define CTRL_ADD_DEVICE			0xfd
#define CANCEL_PRINT			0xf0    // Отменить печать(запись в таблицу ? для поля ? знвчения ?)
#define GET_FN_STATUS			0xf1    // Запрос статуса ФН
#define GET_FN_STATUS_L                 0xFF01  // Запрос статуса ФН
#define GET_FN_NUMBER_L                 0xFF02  // Запрос номера ФН
#define GET_FN_VALIDITY_PERIOD_L        0xFF03  // Запрос срока действия ФН
#define GET_FN_VERSION_L                0xFF04  // Запрос версии ФН
#define BEGIN_KKT_REG_REPORT_L          0xFF05  // Начать отчет о регистрации ККТ
#define FORM_KKT_REG_REPORT_L           0xFF06  // Сформировать отчёт о регистрации ККТ
#define RESET_FN_STATE_L                0xFF07  // Сброс состояния ФН
#define CANCEL_DOC_FN_L                 0xFF08  // Отменить документ в ФН
#define REQUEST_FISCALIZATION_RESULT_L  0xFF09  // Запрос итогов фискализации
#define FIND_FISCAL_DOC_BY_NUMBER_L     0xFF0A  // Найти фискальный документ по номеру
/*
FF0C    // Передать произвольную TLV структуру(Данные документа ФН в формате TLV (согласно документу ФНС «Форматы фискальных документов»). Например, чтобы передать тэг 1008 «ТЕЛ. ПОКУПАТЕЛЯ» или «ЭЛ. АДР. ПОКУПАТЕЛЯ» со значением 12345678 следует записать в TLVData следующую последовательность байт:F0h 03h 08h 00h 31h 32h 33h 34h 35h 36h 37h 38h, где F0h03h – код тэга, 08h00h – длина сообщения.)
FF0D    // Операция со скидками и надбавками
FF34h   // Сформировать отчёт о перерегистрации ККТ
FF35h   // Начать формирование чека коррекции
FF36h   // Сформировать чек коррекции FF36H
FF37h   // Начать формирование отчёта о состоянии расчётов
FF38h   // Сформировать отчёт о состоянии расчётов
 */
#define GET_STATE_INFORM_EXCHANGE_L           0xFF39   // Получить статус информационного обмена
/*
FF3Ah   // Запросить фискальный документ в TLV формате
FF3Bh   // Чтение TLV фискального документа
*/
#define REQUEST_RECEIPT_FROM_OFD_BY_DOC_NUM_L 0xFF3C  // Запрос квитанции о получении данных в ОФД по номеру документа
#define BEGIN_CLOSE_FISCAL_MODE_L             0xFF3D  // Начать закрытие фискального режима
#define CLOSE_FISCAL_MODE_L                   0xFF3E  // Закрыть фискальный режим
#define REQUEST_COUNT_FD_WITHOUT_RECEIPT_L    0xFF3F  // Запрос количества ФД на которые нет квитанции
#define GET_PARAMS_CUR_SHIFT_L                0xFF40  // Запрос параметров текущей смены
#define BEGIN_OPEN_SHIFT_L                    0xFF41  // Начать открытие смены
#define BEGIN_CLOSE_SHIFT_L                   0xFF42  // Начать закрытие смены

#define GET_BUFFER_SIZE_L                     0xFF30  // Запрос о наличие данных в буфере
#define READ_BLOCK_FROM_BUFFER_L              0xFF31  // Прочитать блок данных из буфера

/**
 * PAYONLINE-01-ФА
 * 255
 * Поддерживаемые команды
 * 01h, 02h, 03h, 10h, 11h, 12h, 13h, 14h, 15h, 16h, 17h, 18h,
 * 19h, 1Ah, 1Bh, 1Eh, 1Fh, 21h, 22h, 23h, 24h, 25h, 26h, 27h,
 * 28h, 29h, 2Bh, 2Ch, 2Dh, 2Eh, 2F h , 40h, 41h, 42h, 43h, 44h,
 * 50h, 51h, 52h, 53h, 54h, 60h, 80h, 81h, 82h, 83h, 84h, 85h,
 * 86h, 87h, 88h, 89h, 8Ah, 8Bh, 8Ch, 8Dh, 8Eh, B0h, C0h,
 * C1h, C2h, C3h, C4h, C5h, DDh, DEh, E0h, FCh, FF01H,
 * FF02H, FF03H, FF04H, FF05H, FF06H, FF07H, FF08H, FF09H,
 * FF0АH, FF0СH, FF0DH, FF34H, FF35H, FF36H, FF37H,
 * FF38H, FF39H, FF3AH, FF3BH, FF3CH, FF3DH, FF3EH,
 * FF3FH, FF40H, FF41H, FF42H
 */

struct CommandAnswerResult {
    unsigned char *cmd;
    unsigned char *ans;
    int res;
};

/****************************************************
 * FR properties structure                          *
 * Will be included into class declaration in future*
 ****************************************************/

typedef struct
{
  int    BatteryCondition;
  int    BaudRate;
  char*  BmpImage;
  int    BmpImageSquard;
  char*  BarCode;
  int    RepiatLine;
  string BitString;
  string BarCodeLine;
  // Загрузка данных
  unsigned int    DownloadDataType;// 
  unsigned char*  DownloadData;//Данные
  unsigned int    DownloadDataBlockNumber;//Порядковый номер блока данных
  // печать двухмерного штрих-кода
  /* Тип штрих-кода 
   * 0 - PDF 417
   * 1 - DATAMATRIX
   * 2 - AZTEC
   * 3 - QR code
   * 131 - QR code
   */
  int BarCodeType;             // Тип штрих-кода
  int BarCodeDataLength;       // Длина данных штрих-кода
  int BarCodeStartBlockNumber; // Номер начального блока
/*
 * Номер параметра | PDF 417                 | DATAMATRIX      | AZTEC                  | QR Code
 */
/* Параметр 1 (1 байт) 
 *        1        | Number of columns       | Encoding scheme | Encoding scheme        | Version,0=auto; 40 (max)
 */
  int BarcodeParameter1;       // Параметр штрих-кода 1
/* Параметр 2 (1 байт) 
 *        2        | Number of rows          | Rotate          | -                      | Mask; 8 (max)
 */
  int BarcodeParameter2;       // Параметр штрих-кода 2
/* Параметр 3 (1 байт) 
 *        3        | Width of module         | Dot size        | Dot size               | Dot size; 3...8
 */
  int BarcodeParameter3;       // Параметр штрих-кода 3
/* Параметр 4 (1 байт) 
 *        4        | Module height           | Symbol size     | Symbol size            | -
 */
  int BarcodeParameter4;       // Параметр штрих-кода 4
/* Параметр 5 (1 байт) 
 *        5        | Error correction level  | -               | Error correction level | Error correction level; 0...3=L,M,Q,H
 */
  int BarcodeParameter5;       // Параметр штрих-кода 5
/* Выравнивание (1 байт) 
 * 0 - По левому краю
 * 1 - По центру
 * 2 - По правому краю
 */
  int BarCodeAlignment;        // Выравнивание штрих-кода

  double Change;
  int    CheckIsClosed;
  int    CheckIsMadeOut;
  int    ComPortNumber;
  char*  ComPortString;
  double ContentsOfCashRegister;
  int    ContentsOfOperationRegister;
  int    CurrentDozeInMilliliters;
  double CurrentDozeInMoney;
  int    CutType; // Признак типа отрезки чека: TRUE – неполная отрезка, FALSE – полная отрезка.
  int    CheckType;// Тип открываемого документа/чека 0...3: «0» - продажа, «1» - покупка, «2» - возврат продажи, «3» - возврат покупки.
  unsigned char* DataBlock;
  int    DataBlockNumber;
  struct tm Date;
  int    Department;
  int    DeviceCode;
  const char*  DeviceCodeDescription;
  double DiscountOnCheck;
  char*  DocumentName;
  int    DocumentNumber; // Номер ФД | Номер последнего ФД
  int    DocumentType;
  int    OpenDocumentNumber; // Сквозной номер текущего документа
  int    DozeInMilliliters;
  double DozeInMoney;
  int    DrawerNumber;
  int    ECRAdvancedMode;
  const char*  ECRAdvancedModeDescription;
  int    ECRBuild;
  int    ECRMode; // Режим ККТ
  int    ECRMode8Status;
  const char*  ECRModeDescription;
  
  int    ECRSubmode; // Подрежим ККТ
  const char*  ECRSubmodeDescription; // Подрежим ККТ описание
  unsigned int ECRCountOperationInReceiptHight; // Количество операций в чеке старший байт двухбайтного числа
  unsigned int ECRCountOperationInReceiptLow; // Количество операций в чеке младший байт двухбайтного числа
  int ECRVoltageOfReservBattary; // Напряжение резервной батареи
  int ECRVoltageOfSource; // Напряжение источника питания
  unsigned int ECRErrorCodeFP2; // Код ошибки ФП 2
  unsigned int ECRErrorCodeEKLZ2; // Код ошибки ЭКЛЗ 2
  unsigned int ECRLastPrintResult; // Результат последней печати
  unsigned int ECRFlags; // Флаги ККТ
  
  
  struct tm ECRSoftDate;
  char*  ECRSoftVersion;
  int    EKLZIsPresent;
  int    EmergencyStopCode;
  char*  EmergencyStopCodeDescription;
  char*  FieldName;
  int    FieldNumber;
  int    FieldSize;
  int    FieldType;
  int    FirstLineNumber;
  struct tm FirstSessionDate;
  int    FirstSessionNumber;
  int    FM1IsPresent;
  int    FM2IsPresent;
  int    FMBuild;
  int    FMOverflow;
  struct tm FMSoftDate;
  char*  FMSoftVersion;
  int    FreeRecordInFM;
  int    FreeRegistration;
  char*  INN;
  int    JournalRibbonIsPresent;
  int    JournalRibbonOpticalSensor;
  int    JournalRibbonLever;
  int    KPKNumber;
  int    LastFMRecordType;
  int    LastLineNumber;
  struct tm LastSessionDate;
  int    LastSessionNumber;
  char*  License;
  int    LicenseIsPresent;
  int    LidPositionSensor;
  int    LogicalNumber;
  int    LineNumber;
  unsigned char* LineData;
  unsigned char* LineDataExt;
  int    MAXValueOfField; // Максимальное значение поля внутренней таблицы настроек ККМ, если данное поле типа BIN(числовое)
  int    MINValueOfField; // Минимальное значение поля внутренней таблицы настроек ККМ, если данное поле типа BIN(числовое)
  int    Motor;
  char*  NameCashReg; // Наименование денежного регистра – строка символов в кодировке WIN1251
  char*  NameOperationReg; // Наименование операционного регистра – строка символов в кодировке WIN1251
  int    OperatorNumber; // Порядковый номер оператора, чей пароль был введен.
  int    Password; // Пароль оператора для исполнения метода драйвера.
  int    PasswordAdmin; // Пароль системного администратора default 30
  int    PasswordTI; // ПарольНИ default 0
  int    NewPasswordTI; // НовыйПарольНИ default 0
  int    SCPassword;// ПарольЦТО default 30
  int    NewSCPassword;// НовыйПарольЦТО default 30
  int    Pistol;
  int    PointPosition; // ПоложениеТочки Признак положения десятичной точки. 0 – десятичная точка отделяет 0 разрядов, 1 – десятичная точка отделяет 2 разряда.
  int    PortNumber;// НомерПорта свойство обозначает порт ККМ, через который она подключена к ПК или какому-либо другому устройству. Диапазон значений: 0..255 (0 – порт 1, 1 – порт 2, 2 – порт 3 и т.д.)
  double Price; // 
  double Quantity; // 
  int    ReceiptRibbonIsPresent; // Признак наличия в ККМ рулона чековой ленты. 0 – рулона чековой ленты нет, 1 – рулон чековой ленты есть.
  int    ReceiptRibbonOpticalSensor; // Признак прохождения чековой ленты под оптическим датчиком чековой ленты. 0 – чековой ленты нет под оптическим датчиком; 1 – чековая лента проходит под оптическим датчиком.
  int    ReceiptRibbonLever; // Признак положения рычага термоголовки чековой ленты. 1 – рычаг термоголовки чековой ленты поднят; 0 – рычаг термоголовки чековой ленты опущен.
  int    Timeout;
  int    RegisterNumber; // Номер регистра в командах работы с денежными или операционными регистрами. Диапазон значений: 0..255.
  int    ReportType; // Признак типа отчета: 1 – полный, 0 – короткий.
  int    RegistrationNumber; // Количество перерегистраций (фискализаций), проведенных на ККМ. Диапазон значений: 0..16.
  int    ResultCode; // Свойство содержит код ошибки, возвращаемый ККМ в результате выполнения последней операции. Если ошибки не произошло, то значение данного свойства устанавливается в 0 (Ошибок нет)
  const char*  ResultCodeDescription; // Свойство содержит строку с описанием на русском языке кода ошибки, возникшей в результате последней операции
  int    RKNumber;
  char*  RNM; // Текстовый параметр (строка), содержащий регистрационный номер машины. Максимальная допустимая длина строки: 10(5 байт) или 14(7 байт) символов. Разрешены только символы «0», «1», «2», «3», «4», «5», «6», «7», «8» и «9» (WIN1251-коды цифр)
  int    RoughValve;
  int    RowNumber; // Номер ряда (количество рядов) внутренней таблицы настроек ККМ Диапазон значений: 1..255.
  int    RunningPeriod; // Период вывода тестового чека в минутах в режиме тестового прогона. Диапазон значений: 1..99.
  char*  SerialNumber; // Заводской номер ФН
  int    SessionNumber;
  int    SlipDocumentIsMoving;
  int    SlipDocumentIsPresent;
  int    SlowingInMilliliters;
  int    SlowingValve;
  int    StatusRK;
  char*  StatusRKDescription;
  char*  StringForPrinting;
  int    StringQuantity;
  double Summ1; // Свойство, используемое для хранения наличных значений денежных сумм.
  double Summ2;
  double Summ3;
  double Summ4;
  char*  TableName; // Наименование внутренней таблицы настроек ККМ – строка символов в кодировке WIN1251
/**
 * Номер внутренней таблицы настроек ККМ.
 *  1  - Тип и режимы кассы
 *  2  - Пароли кассиров и администраторов
 *  3  - Таблица перевода времени
 *  4  - Текст в чеке
 *  5  - Наименования типов оплаты
 *  6  - Налоговые ставки
 *  7  - Наименования отделов
 *  8  - Настройка шрифтов
 *  9  - Таблица формата чека
 *  10 - Конфигурация подкладного документа
 *  11 - Межстрочные интервалы подкладного документа
 *  12 - Настройки стандартного фискального подкладного документа
 *  13 - Стандартная операция на подкладном документе
 *  14 - Стандартное закрытие чека на подкладном документе
 *  15 - Стандартная скидка/ надбавка на подкладном документе
 */
  int    TableNumber; // Номер внутренней таблицы настроек ККМ.
  
  int    Tax1; // 1-ый номер налоговой группы.
  int    Tax2; // 2-ый номер налоговой группы.
  int    Tax3; // 3-ый номер налоговой группы.
  int    Tax4; // 4-ый номер налоговой группы.
  struct tm Time;
  int    TRKNumber;
  int    TypeOfSumOfEntriesFM; // Признак суммы записей ФП: 1 – сумма записей после последней перерегистрации, 0 – сумма всех записей
  int    UseJournalRibbon;// Признак операции с лентой операционного журнала. 0 – не производить операцию над лентой операционного журнала, 1 – производить операцию над лентой операционного журнала.
  int    UseReceiptRibbon;// Признак операции с чековой лентой. 0 – не производить операцию над чековой лентой, 1 – производить операцию над чековой лентой.
  int    UseSlipDocument; // Признак операции с подкладным документом. 0 – не производить операцию над подкладным документом, 1 – производить операцию над подкладным документом.
  int    UModel; // Модель устройства подключенного к установленному в драйвере COM-порту
  int    UMajorType; // Тип устройства, подключенного к установленному в драйвере COM-порту
  int    UMinorType; // Подтип устройства подключенного к установленному в драйвере COM-порту
  int    UMajorProtocolVersion; // Версия протокола связи с ПК, используемая устройством
  int    UMinorProtocolVersion; // Подверсия протокола связи с ПК, используемая устройством
  int    UCodePage; // Кодовая страница, используемая устройством (0 – русский;1 – английский;2 – эстонский;3 – казахский;4 – белорусский;5 – армянский;6 – грузинский;7 – украинский;8 – киргизский;9 – туркменский;10 – молдавский;)
  char*  UDescription; // Название устройства – строка символов таблицы WIN1251.
  int    ValueOfFieldInteger;
  char*  ValueOfFieldString;
  /*
   * Состояние смены: 1 байт
   *  00 – смена закрыта
   *  01 – смена открыта
   */
  int    ShiftStatus; // Состояние смены
  /*
   * Состояние смены ФН
   *    0 – смена закрыта;
   *    1 – смена открыта.
   */
  unsigned int FNSessionState; // Состояние смены
  char*  Link;
  
  
  /*
   * Состояние жизни ФН
   *    0 0 0 0 - Настройка (0)
   *    0 0 0 1 - Готовность к фискализации (1)
   *    0 0 1 1 - Фискальный режим (3)
   *    0 1 1 1 - Фискальный режим закрыт, идет передача ФД в ОФД (7)
   *    1 1 1 1 - Чтение данных из Архива ФН (15)
   */
  unsigned int FNLifeState; // Состояние жизни ФН
  /*
   * Текущий документ ФН
   *    00h – нет открытого документа;
   *    01h – отчёт о фискализации;
   *    02h – отчёт об открытии смены;
   *    04h – кассовый чек;
   *    08h – отчёт о закрытии смены;
   *    10h – отчёт о закрытии фискального режима/
   */
  unsigned int FNCurrentDocument; // Текущий документ ФН
  /*
   * Данные документа ФН
   *    0 – нет данных документа;
   *    1 – получены данные документа.
   */
  unsigned int FNDocumentData; // Данные документа
 
  
  /*
   * Флаги предупреждения ФН
   *    0 0 0 1 - Срочная замена криптографического сопроцессора(до окончания срока действия 3 дня)
   *    0 0 1 0 - Исчерпание ресурса криптографического сопроцессора(до окончания срока действия 30 дней)
   *    0 1 0 0 - Переполнение памяти ФН(Архив ФН заполнен на 90%)
   *    1 0 0 0 - Превышено время ожидания ответа ОФД
   */
  unsigned int FNWarningFlags; // Флаги предупреждения
  
  // перечень запросов к устройству за сессию
  //std::vector<CommandAnswerResult> Requests;
  
  //
  //unsigned char *LastCommand;
  //unsigned char *LastAnswer;
  
  unsigned int BufferSize; // кол-во быйт в буфере
  unsigned int BufferBlockMaxSize; // Максимальный размер блока данных
  
} fr_prop;

/****************************************************
 * FR interface functions  & properties structure   *
 * Will be removed in future                        *
 ****************************************************/

struct fr_func
  {
    int (*Connect)(void);
    int (*DisConnect)(void);
    int (*Beep)(void);
    int (*Buy)(void);
    int (*CancelCheck)(void);
    int (*CashIncome)(void);
    int (*CashOutcome)(void);
    int (*Charge)(void);
    int (*CheckSubTotal)(void);
    int (*OpenCheck)(void);
    int (*CloseCheck)(void);
    int (*ConfirmDate)(void);
    int (*ContinuePrinting)(void);
    int (*Correction)(void);
    int (*CutCheck)(void);
    int (*DampRequest)(void);
    int (*Discount)(void);
    int (*DozeOilCheck)(void);
    int (*Draw)(void);
    int (*DrawExt)(void);
    int (*EjectSlipDocument)(void);
    int (*EKLZDepartmentReportInDatesRange)(void);
    int (*EKLZDepartmentReportInSessionsRange)(void);
    int (*EKLZJournalOnSessionNumber)(void);
    int (*EKLZSessionReportInDatesRange)(void);
    int (*EKLZSessionReportInSessionRange)(void);
    int (*FeedDocument)(void);
    int (*Fiscalization)(void);
    int (*FiscalReportForDatesRange)(void);
    int (*FiscalReportForSessionRange)(void);
    int (*GetData)(void);
    int (*GetDeviceMetrics)(void);
    int (*GetExchangeParam)(void);
    int (*GetFieldStruct)(void);
    int (*GetFiscalizationParameters)(void);
    int (*GetFMRecordsSum)(void);
    int (*GetShortStatus)(void);
    int (*GetECRStatus)(void);
    int (*GetFNStatus)(void);
    int (*GetLastFMRecordDate)(void);
    int (*GetLiterSumCounter)(void);
    int (*GetCashReg)(void);
    int (*GetOperationReg)(void);
    int (*GetRangeDatesAndSessions)(void);
    int (*GetRKStatus)(void);
    int (*GetTableStruct)(void);
    int (*InitFM)(void);
    int (*InitTable)(void);
    int (*InterruptDataStream)(void);
    int (*InterruptFullReport)(void);
    int (*InterruptTest)(void);
    int (*LaunchRK)(void);
    int (*LoadLineData)(void);
    int (*LoadLineDataExt)(void);
    int (*OilSale)(void);
    int (*OpenDrawer)(void);
    int (*PrintBarCode)(void);
    int (*PrintBmpImage)(void);
    int (*PrintLine)(void);
    int (*PrintBarCodeMultidimensional)(void);
    std::string  (*PrintBarCode128ByLine)(void);
    std::string  (*PrintQRCode)(void);
    int (*PrintDocumentTitle)(void);
    int (*PrintOperationReg)(void);
    int (*PrintReportWithCleaning)(void);
    int (*PrintReportWithoutCleaning)(void);
    int (*PrintReportWithCleaningToBuffer)(void);
    int (*PrintString)(void);
    int (*PrintWideString)(void);
    int (*ReadEKLZDocumentOnKPK)(void);
    int (*ReadEKLZSessionTotal)(void);
    int (*ReadLicense)(void);
    int (*ReadTable)(void);
    int (*ResetAllTRK)(void);
    int (*ResetRK)(void);
    int (*ResetSettings)(void);
    int (*ResetSummary)(void);
    int (*ReturnBuy)(void);
    int (*ReturnSale)(void);
    int (*Sale)(void);
    int (*SetDate)(void);
    int (*SetDozeInMilliliters)(void);
    int (*SetDozeInMoney)(void);
    int (*SetExchangeParam)(void);
    int (*SetPointPosition)(void);
    int (*SetRKParameters)(void);
    int (*SetSerialNumber)(void);
    int (*SetTime)(void);
    int (*StopEKLZDocumentPrinting)(void);
    int (*StopRK)(void);
    int (*Storno)(void);
    int (*StornoCharge)(void);
    int (*StornoDiscount)(void);
    int (*SummOilCheck)(void);
    int (*Test)(void);
    int (*WriteLicense)(void);
    int (*WriteTable)(void);
    int (*OpenShift)(void);
    int (*CancelPrint)(void);
    int (*ResetFnState)(void);
    int (*GetInformExchangeStatus)(void);
    int (*DownloadData)(void);
    int (*SetPrintReceiptToOut)(void);
    int (*SetPrintReportToOut)(void);
    int (*GetPrintReceiptToOut)(void);
    int (*GetPrintReportToOut)(void);
    int (*GetBufferSize)(void);
    int (*ReadBlockFromBuffer)(void);
    int (*PrintFromBuffer)(void);
    /* Pointer to the interface properties structure */
    fr_prop *prop;
  };

/****************************************************
 * Returns the pointer to the properties structure. *
 * Will be removed by class constructor in future   *
 ****************************************************/

fr_func *drvfrInitialize(void);

#endif
