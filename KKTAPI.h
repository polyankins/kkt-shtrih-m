/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   KKTAPI.h
 * Author: semen
 *
 * Created on 13 марта 2018 г., 10:42
 */

#ifndef KKTAPI_H
#define KKTAPI_H

#include <map>
#include <string>
#include <string.h>


#include "errdefs.h"
#include "conn.h"
#include "options.h"
#include "drvfr.h"
#include "interface.h"

struct PtCharCompare : public std::binary_function<const char *, const char *, bool> {
public:
    bool operator() (const char * str1, const char * str2) const
    { return strcmp(str1, str2) > 0; }
};


typedef std::map<const char *, std::string (*)(fr_func*, fr_prop*, std::map<const char *, const char *>), PtCharCompare> ApiMethods;

class KKTAPI {
protected:

private:
    
    KKTAPI();
    KKTAPI(const KKTAPI& orig);
    ~KKTAPI();
    
    static ApiMethods _api_methods;
public:
    
    static void init_map_methods();
    
    static bool has_api_method( const char * );
    
    static std::string json_head(fr_func*);
    static std::string json_foot(fr_func*);
    static std::string api_method( const char *, fr_func*, fr_prop*, const char *);
    
    static std::string api_get_short_status(fr_func*, fr_prop*, std::map<const char *, const char *>);
    static std::string api_get_ecr_status(fr_func*, fr_prop*, std::map<const char *, const char *>);
    static std::string api_get_fn_status(fr_func*, fr_prop*, std::map<const char *, const char *>);
    static std::string api_clear_fn_status(fr_func*, fr_prop*, std::map<const char *, const char *>);
    
    static ApiMethods get_api_methods();
    
};

#endif /* KKTAPI_H */

