/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   bmp.cpp
 * Author: semen
 *
 * Created on 13 февраля 2018 г., 18:01
 */

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include "bmp.h"

using namespace std;

static RGBQUAD** read_bmp(const char *filePath, BITMAPFILEHEADER &fileHeader, BITMAPINFOHEADER &fileInfoHeader) {
    std::cout << "~~~ read_bmp ~~~" << std::endl;
    // открываем файл
    std::ifstream fileStream(filePath, std::ifstream::binary);
    if (!fileStream) {
        std::cout << "~ Error opening file '" << filePath << "'." << std::endl;
        return NULL;
    } else {
        std::cout << "~ Succes opening file '" << filePath << "'." << std::endl;
    }

    // заголовк изображения
    //BITMAPFILEHEADER fileHeader;
    read(fileStream, fileHeader.bfType, sizeof (fileHeader.bfType));
    std::cout << "~ Succes read file header bfType " << fileHeader.bfType << std::endl;
    read(fileStream, fileHeader.bfSize, sizeof (fileHeader.bfSize));
    std::cout << "~ Succes read file header bfSize " << fileHeader.bfSize << std::endl;
    read(fileStream, fileHeader.bfReserved1, sizeof (fileHeader.bfReserved1));
    std::cout << "~ Succes read file header bfReserved1 " << fileHeader.bfReserved1 << std::endl;
    read(fileStream, fileHeader.bfReserved2, sizeof (fileHeader.bfReserved2));
    std::cout << "~ Succes read file header bfReserved2 " << fileHeader.bfReserved2 << std::endl;
    read(fileStream, fileHeader.bfOffBits, sizeof (fileHeader.bfOffBits));
    std::cout << "~ Succes read file header bfOffBits " << fileHeader.bfOffBits << std::endl;



    if (fileHeader.bfType != 0x4D42) {
        std::cout << "~ Error: '" << filePath << "' is not BMP file." << std::endl;
        return NULL;
    }

    // информация изображения
    //BITMAPINFOHEADER fileInfoHeader;
    read(fileStream, fileInfoHeader.biSize, sizeof (fileInfoHeader.biSize));
    std::cout << "~ Succes read file info header biSize " << fileInfoHeader.biSize << std::endl;

    // bmp core
    if (fileInfoHeader.biSize >= 12) {
        std::cout << "~ bmp core ~" << std::endl;
        read(fileStream, fileInfoHeader.biWidth, sizeof (fileInfoHeader.biWidth));
        std::cout << "~ Succes read file info header biWidth " << fileInfoHeader.biWidth << std::endl;
        read(fileStream, fileInfoHeader.biHeight, sizeof (fileInfoHeader.biHeight));
        std::cout << "~ Succes read file info header biHeight " << fileInfoHeader.biHeight << std::endl;
        read(fileStream, fileInfoHeader.biPlanes, sizeof (fileInfoHeader.biPlanes));
        std::cout << "~ Succes read file info header biPlanes " << fileInfoHeader.biPlanes << std::endl;
        read(fileStream, fileInfoHeader.biBitCount, sizeof (fileInfoHeader.biBitCount));
        std::cout << "~ Succes read file info header biBitCount " << fileInfoHeader.biBitCount << std::endl;
    }

    if ( fileInfoHeader.biBitCount != 1 && fileInfoHeader.biBitCount != 16
            && fileInfoHeader.biBitCount != 24 && fileInfoHeader.biBitCount != 32
            ) {
        std::cout << "~ Error: Unsupported BMP bit count." << std::endl;
        return 0;
    }
    std::cout << "~ BIT COUNT SUPPORTED" << std::endl;
    
    // получаем информацию о битности
    int colorsCount = 1;
    if( fileInfoHeader.biBitCount > 1 ){
        colorsCount = fileInfoHeader.biBitCount >> 3;
        if (colorsCount < 3) {
            colorsCount = 3;
        }
    }
    std::cout << "~ colorsCount " << colorsCount << std::endl;

    int bitsOnColor = fileInfoHeader.biBitCount / colorsCount;
    int maskValue = (1 << bitsOnColor) - 1;

    // bmp v1
    if (fileInfoHeader.biSize >= 40) {
        std::cout << "~ bmp v1 ~" << std::endl;
        read(fileStream, fileInfoHeader.biCompression, sizeof (fileInfoHeader.biCompression));
        std::cout << "~ Succes read file info header biCompression " << fileInfoHeader.biCompression << std::endl;
        read(fileStream, fileInfoHeader.biSizeImage, sizeof (fileInfoHeader.biSizeImage));
        std::cout << "~ Succes read file info header biSizeImage " << fileInfoHeader.biSizeImage << std::endl;
        read(fileStream, fileInfoHeader.biXPelsPerMeter, sizeof (fileInfoHeader.biXPelsPerMeter));
        std::cout << "~ Succes read file info header biXPelsPerMeter " << fileInfoHeader.biXPelsPerMeter << std::endl;
        read(fileStream, fileInfoHeader.biYPelsPerMeter, sizeof (fileInfoHeader.biYPelsPerMeter));
        std::cout << "~ Succes read file info header biYPelsPerMeter " << fileInfoHeader.biYPelsPerMeter << std::endl;
        read(fileStream, fileInfoHeader.biClrUsed, sizeof (fileInfoHeader.biClrUsed));
        std::cout << "~ Succes read file info header biClrUsed " << fileInfoHeader.biClrUsed << std::endl;
        read(fileStream, fileInfoHeader.biClrImportant, sizeof (fileInfoHeader.biClrImportant));
        std::cout << "~ Succes read file info header biClrImportant " << fileInfoHeader.biClrImportant << std::endl;
    }

    // bmp v2
    fileInfoHeader.biRedMask = 0;
    fileInfoHeader.biGreenMask = 0;
    fileInfoHeader.biBlueMask = 0;

    if (fileInfoHeader.biSize >= 52) {
        std::cout << "~ bmp v2 ~" << std::endl;
        read(fileStream, fileInfoHeader.biRedMask, sizeof (fileInfoHeader.biRedMask));
        std::cout << "~ Succes read file info header biRedMask " << fileInfoHeader.biRedMask << std::endl;
        read(fileStream, fileInfoHeader.biGreenMask, sizeof (fileInfoHeader.biGreenMask));
        std::cout << "~ Succes read file info header biGreenMask " << fileInfoHeader.biGreenMask << std::endl;
        read(fileStream, fileInfoHeader.biBlueMask, sizeof (fileInfoHeader.biBlueMask));
        std::cout << "~ Succes read file info header biBlueMask " << fileInfoHeader.biBlueMask << std::endl;
    }

    // если маска не задана, то ставим маску по умолчанию
    if (fileInfoHeader.biRedMask == 0 || fileInfoHeader.biGreenMask == 0 || fileInfoHeader.biBlueMask == 0) {
        fileInfoHeader.biRedMask = maskValue << (bitsOnColor * 2);
        fileInfoHeader.biGreenMask = maskValue << bitsOnColor;
        fileInfoHeader.biBlueMask = maskValue;
    }

    // bmp v3
    if (fileInfoHeader.biSize >= 56) {
        std::cout << "~ bmp v3 ~" << std::endl;
        read(fileStream, fileInfoHeader.biAlphaMask, sizeof (fileInfoHeader.biAlphaMask));
        std::cout << "~ Succes read file info header biAlphaMask " << fileInfoHeader.biAlphaMask << std::endl;
    } else {
        fileInfoHeader.biAlphaMask = maskValue << (bitsOnColor * 3);
    }

    // bmp v4
    if (fileInfoHeader.biSize >= 108) {
        std::cout << "~ bmp v4 ~" << std::endl;
        read(fileStream, fileInfoHeader.biCSType, sizeof (fileInfoHeader.biCSType));
        std::cout << "~ Succes read file info header biCSType " << fileInfoHeader.biCSType << std::endl;
        read(fileStream, fileInfoHeader.biEndpoints, sizeof (fileInfoHeader.biEndpoints));
        std::cout << "~ Succes read file info header biEndpoints " /*<< fileInfoHeader.biEndpoints */ << std::endl;
        read(fileStream, fileInfoHeader.biGammaRed, sizeof (fileInfoHeader.biGammaRed));
        std::cout << "~ Succes read file info header biGammaRed " << fileInfoHeader.biGammaRed << std::endl;
        read(fileStream, fileInfoHeader.biGammaGreen, sizeof (fileInfoHeader.biGammaGreen));
        std::cout << "~ Succes read file info header biGammaGreen " << fileInfoHeader.biGammaGreen << std::endl;
        read(fileStream, fileInfoHeader.biGammaBlue, sizeof (fileInfoHeader.biGammaBlue));
        std::cout << "~ Succes read file info header biGammaBlue " << fileInfoHeader.biGammaBlue << std::endl;
    }

    // bmp v5
    if (fileInfoHeader.biSize >= 124) {
        std::cout << "~ bmp v5 ~" << std::endl;
        read(fileStream, fileInfoHeader.biIntent, sizeof (fileInfoHeader.biIntent));
        std::cout << "~ Succes read file info header biIntent " << fileInfoHeader.biIntent << std::endl;
        read(fileStream, fileInfoHeader.biProfileData, sizeof (fileInfoHeader.biProfileData));
        std::cout << "~ Succes read file info header biProfileData " << fileInfoHeader.biProfileData << std::endl;
        read(fileStream, fileInfoHeader.biProfileSize, sizeof (fileInfoHeader.biProfileSize));
        std::cout << "~ Succes read file info header biProfileSize " << fileInfoHeader.biProfileSize << std::endl;
        read(fileStream, fileInfoHeader.biReserved, sizeof (fileInfoHeader.biReserved));
        std::cout << "~ Succes read file info header biReserved " << fileInfoHeader.biReserved << std::endl;
    }

    // проверка на поддерку этой версии формата
    if (fileInfoHeader.biSize != 12 && fileInfoHeader.biSize != 40 && fileInfoHeader.biSize != 52 &&
            fileInfoHeader.biSize != 56 && fileInfoHeader.biSize != 108 && fileInfoHeader.biSize != 124) {
        std::cout << "~ Error: Unsupported BMP format." << std::endl;
        return 0;
    }
    std::cout << "~ VERSION SUPPORTED" << std::endl;

    if (fileInfoHeader.biCompression != 0 && fileInfoHeader.biCompression != 3) {
        std::cout << "~ Error: Unsupported BMP compression." << std::endl;
        return 0;
    }

    std::cout << "~ COMPRESSION SUPPORTED" << std::endl;

    // rgb info
    RGBQUAD **rgbInfo = new RGBQUAD*[fileInfoHeader.biHeight];

    for (unsigned int i = 0; i < fileInfoHeader.biHeight; i++) {
        rgbInfo[i] = new RGBQUAD[fileInfoHeader.biWidth];
    }

    std::cout << "~ rgbInfo array generated" << std::endl;

    // определение размера отступа в конце каждой строки
    int linePadding = ((fileInfoHeader.biWidth * (fileInfoHeader.biBitCount / 8)) % 4) & 3;

    std::cout << "~ padding on end line " << linePadding << std::endl;

    // чтение
    unsigned int bufer;

    std::cout << "~ start read RGB DATA size is " << std::dec << fileInfoHeader.biWidth << " x " << fileInfoHeader.biHeight << std::endl;
    for (unsigned int i = 0; i < fileInfoHeader.biHeight; i++) {
        for (unsigned int j = 0; j < fileInfoHeader.biWidth; j++) {

            if( fileInfoHeader.biBitCount > 1 ){
                read(fileStream, bufer, fileInfoHeader.biBitCount / 8);
                std::cout << bufer << " | ";
                rgbInfo[i][j].rgbRed = bitextract(bufer, fileInfoHeader.biRedMask);
                rgbInfo[i][j].rgbGreen = bitextract(bufer, fileInfoHeader.biGreenMask);
                rgbInfo[i][j].rgbBlue = bitextract(bufer, fileInfoHeader.biBlueMask);
                rgbInfo[i][j].rgbReserved = bitextract(bufer, fileInfoHeader.biAlphaMask);
            } else {
                read(fileStream, bufer, 1);
                std::cout << std::hex << bufer << " | ";
                rgbInfo[i][j].rgbRed = bufer;
                rgbInfo[i][j].rgbGreen = bufer;
                rgbInfo[i][j].rgbBlue = bufer;
                rgbInfo[i][j].rgbReserved = '0';
            }
            
            std::cout << "~ success read " << std::dec << j << " x " << i << " RGB[" << rgbInfo[i][j].rgbRed << rgbInfo[i][j].rgbGreen << rgbInfo[i][j].rgbBlue << "]R[" << rgbInfo[i][j].rgbReserved << "]" << std::endl;
        }
        if( linePadding > 0 ){
            fileStream.seekg(linePadding, std::ios_base::cur);
        }
    }

    std::cout << "~~~ END read_bmp ~~~" << std::endl;

    return rgbInfo;
}

static unsigned char bitextract(const unsigned int byte, const unsigned int mask) {
    if (mask == 0) {
        return 0;
    }

    // определение количества нулевых бит справа от маски
    int
    maskBufer = mask,
            maskPadding = 0;

    while (!(maskBufer & 1)) {
        maskBufer >>= 1;
        maskPadding++;
    }

    // применение маски и смещение
    return (byte & mask) >> maskPadding;
}

static unsigned short read_u16(FILE *fp) {
    unsigned char b0, b1;

    b0 = getc(fp);
    b1 = getc(fp);

    return ((b1 << 8) | b0);
}

static unsigned int read_u32(FILE *fp) {
    unsigned char b0, b1, b2, b3;

    b0 = getc(fp);
    b1 = getc(fp);
    b2 = getc(fp);
    b3 = getc(fp);

    return ((((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
}

static int read_s32(FILE *fp) {
    unsigned char b0, b1, b2, b3;

    b0 = getc(fp);
    b1 = getc(fp);
    b2 = getc(fp);
    b3 = getc(fp);

    return ((int) (((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
}